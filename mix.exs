defmodule Investfolio.MixProject do
  use Mix.Project

  def project do
    [
      app: :investfolio,
      version: "0.1.0",
      elixir: "~> 1.5",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Investfolio.Application, []},
      extra_applications: [:lager, :logger, :runtime_tools, :amqp]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      # Default Phoenix project no-webpack and no-html
      {:phoenix, "~> 1.4.16"},
      {:phoenix_pubsub, "~> 1.1"},
      {:phoenix_ecto, "~> 4.0"},
      {:ecto_sql, "~> 3.1"},
      {:postgrex, ">= 0.0.0"},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      {:plug_cowboy, "~> 2.0"},
      # GraphQL implementation for Elixir
      {:absinthe, "~> 1.4.0"},
      {:absinthe_plug, "~> 1.4.0"},
      {:absinthe_phoenix, "~> 1.4.0"},
      # Improve queries and avoid N+1
      {:dataloader, "~> 1.0.0"},
      # A custom Ecto type for storing encrypted passwords using Comeonin
      {:comeonin_ecto_password, "~> 3.0.0"},
      {:pbkdf2_elixir, "~> 1.0"},
      # Arbitrary precision decimal arithmetic
      {:decimal, "~> 1.0"},
      # An Elixir Plug to add CORS
      {:cors_plug, "~> 1.5"},
      # A token based authentication library for use with Elixir applications.
      {:guardian, "~> 2.0"},
      # An extension to Guardian that tracks tokens in your application's database to prevent playback.
      {:guardian_db, "~> 2.0"},
      # An Ecto extension to support enums in your Ecto models.
      {:ecto_enum, "~> 1.4"},
      # An HTTP client loosely based on Faraday.
      {:tesla, "~> 1.3.0"},
      # An HTTP client library for Erlang.
      {:hackney, "~> 1.15"},
      # Build concurrent and multi-stage data ingestion and data processing pipelines with Elixir
      {:broadway, "~> 0.6.0"},
      # A RabbitMQ connector for Broadway.
      {:broadway_rabbitmq, "~> 0.6.0"},
      # A set of behaviours meant to be used to create RabbitMQ consumers and publishers.
      {:gen_rmq, "~> 2.5.0"},
      # A mocking library for the Elixir language.
      {:mock, "~> 0.3.0", only: :test},
      # An Elixir library that reports test coverage statistics
      {:excoveralls, "~> 0.10", only: :test}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.drop --quiet", "ecto.create --quiet", "ecto.migrate --quiet", "test"]
    ]
  end
end
