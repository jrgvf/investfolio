defmodule Investfolio.Users.User do
  use Investfolio.Schema

  alias Investfolio.Portfolios.{Portfolio, PortfolioUser}

  schema "users" do
    field :email, :string
    field :email_confirmation, :string, virtual: true
    field :name, :string
    field :password, Comeonin.Ecto.Password
    field :password_confirmation, Comeonin.Ecto.Password, virtual: true

    many_to_many :portfolios, Portfolio, join_through: PortfolioUser

    timestamps()
  end

  @required_fields ~w(name email password)a
  @confirmation_fields ~w(email_confirmation password_confirmation)a
  @email_regex ~r/^[\w.!#$%&’*+\-\/=?\^`{|}~]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*$/i

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
    |> update_change(:email, &String.downcase/1)
    |> validate_format(:email, @email_regex)
    |> unique_constraint(:email)
    |> validate_password(user, attrs)
  end

  @doc false
  def insert_changeset(user, attrs) do
    user
    |> cast(attrs, @required_fields ++ @confirmation_fields)
    |> validate_required(@required_fields ++ @confirmation_fields)
    |> update_change(:email, &String.downcase/1)
    |> validate_format(:email, @email_regex)
    |> validate_confirmation(:email)
    |> validate_confirmation(:password)
    |> unique_constraint(:email)
    |> validate_password(user, attrs)
  end

  def validate_password(changeset, user, attrs) do
    {user, %{password: :string}}
    |> cast(attrs, [:password])
    |> validate_length(:password, min: 8)
    |> merge(changeset)
  end
end
