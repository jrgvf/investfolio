defmodule Investfolio.Users do
  @moduledoc """
  The Users context.
  """

  import Ecto.Query, warn: false
  alias Investfolio.Repo
  alias Comeonin.Ecto.Password

  alias Investfolio.Users.User

  @doc """
  Authenicate and return use always is ok

  ## Examples

      iex> authenticate("foo@bar.com", "foobar")
      {:ok, %User{}}

      iex> authenticate("foo@bar.com", "wrong!")
      :error
  """
  def authenticate(email, password) do
    user = Repo.get_by(User, email: email)

    with %{password: digest} <- user,
         true <- Password.valid?(password, digest) do
      {:ok, user}
    else
      _ ->
        Pbkdf2.no_user_verify()
        :error
    end
  end

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.insert_changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Fetch a user.

  ## Examples

      iex> get_user!(id)
      %User{}

      iex> get_user!(id)
      Ecto.NoResultsError

  """
  def get_user!(id) do
    Repo.get!(User, id)
  end

  @doc """
  Deletes a user.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  List all user portfolios.

  ## Examples

      iex> list_portfolios(user)
      [%Portfolio{}]

      iex> list_portfolios(user)
      []

  """
  def list_portfolios(%User{} = user) do
    Ecto.assoc(user, :portfolios)
    |> order_by(:inserted_at)
    |> Repo.all()
  end
end
