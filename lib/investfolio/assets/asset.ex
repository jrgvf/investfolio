defmodule Investfolio.Assets.Asset do
  use Investfolio.Schema

  alias Investfolio.Assets.TypeEnum

  schema "assets" do
    field :code, :string
    field :company_abv_name, :string
    field :company_name, :string
    field :external_id, :string
    field :name, :string
    field :type, TypeEnum

    timestamps()
  end

  @doc false
  def changeset(asset, attrs) do
    asset
    |> cast(attrs, [:external_id, :code, :name, :company_name, :company_abv_name, :type])
    |> validate_required([:external_id, :code, :name, :company_name, :company_abv_name, :type])
    |> unique_constraint(:code)
    |> unique_constraint(:external_id)
  end
end
