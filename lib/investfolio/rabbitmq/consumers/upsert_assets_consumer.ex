defmodule Investfolio.RabbitMQ.Consumers.UpsertAssetsConsumer do
  use Investfolio.RabbitMQ.Consumer,
    producer: [queue: RabbitMQ.Manager.upsert_assets_queue_name()],
    processors: [default: [concurrency: 50]]

  alias Investfolio.Assets

  @impl true
  def handle_message(_processor, %Message{data: data} = message, _context) do
    with {:ok, %{code: code} = asset_params} <- Jason.decode(data, keys: :atoms),
         {:ok, %Assets.Asset{}} <-
           Assets.create_asset(
             asset_params,
             on_conflict: [set: [code: code]],
             conflict_target: [:code]
           ) do
      message
    end
  end
end
