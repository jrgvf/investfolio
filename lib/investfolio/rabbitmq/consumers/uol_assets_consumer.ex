defmodule Investfolio.RabbitMQ.Consumers.UolAssetsConsumer do
  use Investfolio.RabbitMQ.Consumer,
    producer: [queue: RabbitMQ.Manager.uol_assets_queue_name(), qos: [prefetch_count: 100]],
    processors: [default: [concurrency: 100]],
    batchers: [default: [batch_size: 10, batch_timeout: 10_000, concurrency: 2]]

  alias Investfolio.Uol.{Asset, Client}

  @impl true
  def handle_message(_processor, message, _context) do
    Message.update_data(message, fn encoded_uol_asset ->
      uol_asset =
        encoded_uol_asset
        |> Jason.decode!(keys: :atoms)
        |> Asset.build()

      with {:ok, _} <- Client.asset_intraday(uol_asset.idt, 1) do
        {:ok, uol_asset}
      end
    end)
  end

  @impl true
  def handle_batch(_batcher, messages, _batch_info, _context) do
    messages
    |> Enum.reject(fn
      %Message{data: {:ok, _}} -> false
      %Message{data: {:warning, _}} -> true
      %Message{data: {:error, _}} -> true
    end)
    |> Enum.map(fn %Message{data: {_, uol_asset}} ->
      uol_asset
      |> Asset.prepare_to_schema()
      |> Jason.encode!()
    end)
    |> Enum.each(&RabbitMQ.Manager.publish(queue(), &1))

    messages
  end

  defp queue,
    do: RabbitMQ.Manager.upsert_assets_queue_name()
end
