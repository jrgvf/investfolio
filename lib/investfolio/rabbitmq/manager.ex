defmodule Investfolio.RabbitMQ.Manager do
  @behaviour GenRMQ.Publisher

  @queues Application.fetch_env!(:investfolio, __MODULE__) |> Keyword.get(:queues, [])
  @dead_letter_suffix ".dead-letter"
  @dead_letter_ttl 600_000

  for {fun, queue_name} <- @queues do
    def unquote(:"#{fun}_queue_name")(),
      do: unquote(queue_name)

    def unquote(:"#{fun}_dead_letter_queue_name")(),
      do: unquote(queue_name <> @dead_letter_suffix)
  end

  def init do
    create_resources()
    [uri: rabbitmq_uri(), exchange: exchange()]
  end

  def start_link,
    do: GenRMQ.Publisher.start_link(__MODULE__, name: __MODULE__)

  def queue_size(queue),
    do: GenRMQ.Publisher.message_count(__MODULE__, queue)

  def publish(queue, message, options \\ publish_options()),
    do: GenRMQ.Publisher.publish(__MODULE__, message, queue, options)

  def rabbitmq_uri do
    uri =
      "amqp://" <>
        username() <>
        ":" <>
        password() <>
        "@" <>
        host() <>
        ":" <>
        port()

    case virtual_host() do
      vhost when is_nil(vhost) or vhost == "" -> uri
      vhost when is_bitstring(vhost) -> uri <> "/" <> vhost
      _ -> uri
    end
  end

  defp create_resources do
    rabbitmq_uri()
    |> open_connection()
    |> open_channel()
    |> create_exchanges()
    |> create_and_bind_queues()
    |> AMQP.Channel.close()
  end

  # Setup RabbitMQ connection
  defp open_connection(uri) do
    {:ok, connection} = AMQP.Connection.open(uri)
    connection
  end

  # Setup RabbitMQ channel
  defp open_channel(connection) do
    {:ok, channel} = AMQP.Channel.open(connection)
    channel
  end

  # Create exchange
  defp create_exchanges(channel) do
    exchange = exchange()
    dead_letter_exchange = exchange <> @dead_letter_suffix

    AMQP.Exchange.declare(channel, exchange, :topic, durable: true)
    AMQP.Exchange.declare(channel, dead_letter_exchange, :topic, durable: true)
    channel
  end

  # Create queues and bind queues to exchange
  defp create_and_bind_queues(channel) do
    @queues
    |> Enum.each(fn {_, queue_name} ->
      channel
      |> create_and_bind_queue(queue_name, exchange())
      |> create_and_bind_dead_letter_queue(queue_name, exchange())
    end)

    channel
  end

  defp create_and_bind_queue(channel, queue_name, exchange) do
    arguments = [
      {"x-dead-letter-exchange", :longstr, exchange <> @dead_letter_suffix},
      {"x-dead-letter-routing-key", :longstr, queue_name <> @dead_letter_suffix}
    ]

    AMQP.Queue.declare(channel, queue_name, durable: true, arguments: arguments)
    AMQP.Queue.bind(channel, queue_name, exchange, routing_key: queue_name)
    channel
  end

  defp create_and_bind_dead_letter_queue(channel, queue_name, exchange) do
    arguments = [
      {"x-dead-letter-exchange", :longstr, exchange},
      {"x-dead-letter-routing-key", :longstr, queue_name},
      {"x-message-ttl", :long, @dead_letter_ttl}
    ]

    queue_name = queue_name <> @dead_letter_suffix
    exchange = exchange <> @dead_letter_suffix

    AMQP.Queue.declare(channel, queue_name, durable: true, arguments: arguments)
    AMQP.Queue.bind(channel, queue_name, exchange, routing_key: queue_name)
    channel
  end

  defp username,
    do: fetch_env!() |> Keyword.get(:username, "rabbitmq")

  defp password,
    do: fetch_env!() |> Keyword.get(:password, "rabbitmq")

  defp host,
    do: fetch_env!() |> Keyword.get(:host, "localhost")

  defp port,
    do: fetch_env!() |> Keyword.get(:port, 5672) |> to_string()

  defp virtual_host,
    do: fetch_env!() |> Keyword.get(:virtual_host)

  defp exchange,
    do: fetch_env!() |> Keyword.get(:exchange, "manager")

  defp publish_options,
    do: fetch_env!() |> Keyword.get(:publish_options, persistent: true)

  defp fetch_env!,
    do: Application.fetch_env!(:investfolio, __MODULE__)
end
