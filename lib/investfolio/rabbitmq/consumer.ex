defmodule Investfolio.RabbitMQ.Consumer do
  defmacro __using__(options) do
    quote do
      use Broadway

      alias Broadway.Message
      alias Investfolio.RabbitMQ

      def start_link(_opts) do
        Broadway.start_link(__MODULE__,
          name: __MODULE__,
          producer: producer(Mix.env()),
          processors: Keyword.get(unquote(options), :processors, []),
          batchers: Keyword.get(unquote(options), :batchers, [])
        )
      end

      defp producer(:test),
        do: [module: {Broadway.DummyProducer, []}]

      defp producer(_) do
        producer_config = Keyword.get(unquote(options), :producer, [])

        [
          module: {
            BroadwayRabbitMQ.Producer,
            queue: Keyword.fetch!(producer_config, :queue),
            qos: Keyword.get(producer_config, :qos, prefetch_count: 50),
            on_failure: Keyword.get(producer_config, :on_failure, :reject_and_requeue_once),
            backoff_type: Keyword.get(producer_config, :backoff_type, :exp),
            connection: RabbitMQ.Manager.rabbitmq_uri()
          },
          concurrency: Keyword.get(producer_config, :concurrency, 1)
        ]
      end
    end
  end
end
