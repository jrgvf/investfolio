defmodule Investfolio.Portfolios.Dividend do
  use Investfolio.Schema

  alias Investfolio.Dividends.TypeEnum
  alias Investfolio.Portfolios.Portfolio
  alias Investfolio.Assets.Asset

  schema "dividends" do
    field :asset_code, :string
    field :base_date, :date
    field :isin, :string
    field :payment_date, :date
    field :quantity, :integer
    field :total_received, :decimal, read_after_writes: true
    field :type, TypeEnum, default: :income
    field :value, :decimal
    field :reference_month, :integer, read_after_writes: true
    field :reference_year, :integer, read_after_writes: true

    belongs_to :portfolio, Portfolio
    belongs_to :asset, Asset

    timestamps()
  end

  @required_fields ~w(asset_code type quantity value base_date payment_date portfolio_id asset_id)a
  @optional_fields ~w(isin)a
  @doc false
  def changeset(dividend, attrs) do
    dividend
    |> cast(attrs, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> validate_number(:quantity, greater_than: 0)
    |> validate_number(:value, greater_than: 0)
  end
end
