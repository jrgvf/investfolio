defmodule Investfolio.Portfolios.Portfolio do
  use Investfolio.Schema

  alias Investfolio.Portfolios.{StatusEnum, PortfolioUser, Movement, Dividend}
  alias Investfolio.Users.User

  schema "portfolios" do
    field :description, :string
    field :name, :string
    field :status, StatusEnum, default: :active

    many_to_many :users, User, join_through: PortfolioUser
    has_many :movements, Movement
    has_many :dividends, Dividend

    timestamps()
  end

  @doc false
  def changeset(portfolio, attrs) do
    portfolio
    |> cast(attrs, [:name, :description, :status])
    |> validate_required([:name, :status])
  end
end
