defmodule Investfolio.Portfolios.Movement do
  use Investfolio.Schema

  alias Investfolio.Movements.TypeEnum
  alias Investfolio.Portfolios.Portfolio
  alias Investfolio.Assets.Asset

  schema "movements" do
    field :asset_code, :string
    field :average_price, :decimal
    field :date, :date
    field :description, :string
    field :isin, :string
    field :operational_cost, :decimal
    field :quantity, :integer
    field :total_value, :decimal, read_after_writes: true
    field :type, TypeEnum

    belongs_to :portfolio, Portfolio
    belongs_to :asset, Asset

    timestamps()
  end

  @required_fields ~w(asset_code type quantity date average_price operational_cost portfolio_id asset_id)a
  @optional_fields ~w(isin description)a
  @doc false
  def changeset(movement, attrs) do
    movement
    |> cast(attrs, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> validate_number(:average_price, greater_than: 0)
    |> validate_number(:quantity, greater_than: 0)
    |> validate_number(:operational_cost, greater_than_or_equal_to: 0)
  end
end
