defmodule Investfolio.Portfolios.PortfolioUser do
  use Investfolio.Schema

  alias Investfolio.Users.User
  alias Investfolio.Portfolios.{Portfolio, RoleEnum}

  schema "portfolios_users" do
    field :role, RoleEnum, default: :owner

    belongs_to :user, User
    belongs_to :portfolio, Portfolio

    timestamps()
  end

  @required_attrs ~w(role user_id portfolio_id)a
  @doc false
  def changeset(user_portfolio, attrs) do
    user_portfolio
    |> cast(attrs, @required_attrs)
    |> validate_required(@required_attrs)
  end
end
