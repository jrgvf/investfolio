defmodule Investfolio.Repo do
  use Ecto.Repo,
    otp_app: :investfolio,
    adapter: Ecto.Adapters.Postgres
end
