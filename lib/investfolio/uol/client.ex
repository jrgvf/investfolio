defmodule Investfolio.Uol.Client do
  use Tesla, only: [:get]

  adapter(Tesla.Adapter.Hackney, pool: :uol)

  plug Tesla.Middleware.BaseUrl, "https://cotacoes.economia.uol.com.br/ws"
  plug Tesla.Middleware.FollowRedirects
  plug Tesla.Middleware.PathParams
  plug Tesla.Middleware.Timeout, timeout: 5_000
  # Always before Tesla.Middleware.JSON
  plug Investfolio.Uol.Middleware.UolResponseParser

  plug Tesla.Middleware.JSON,
    engine: Jason,
    engine_opts: [keys: :atoms],
    decode_content_types: ["text/plain", "application/json"]

  plug Tesla.Middleware.Retry,
    delay: 500,
    max_retries: 3,
    max_delay: 1_000,
    should_retry: fn
      {:ok, _} -> false
      {:warning, _} -> false
      {:error, _} -> true
    end

  def assets_list,
    do: get("/asset/stock/list")

  def asset_intraday(id, size \\ nil)

  def asset_intraday(id, nil) do
    params = [id: id]
    get("/asset/:id/intraday", opts: [path_params: params])
  end

  def asset_intraday(id, size) do
    params = [id: id]
    get("/asset/:id/intraday", opts: [path_params: params], query: [size: size])
  end

  def asset_interday(id, size \\ nil)

  def asset_interday(id, nil) do
    params = [id: id]
    get("/asset/:id/interday", opts: [path_params: params])
  end

  def asset_interday(id, size) do
    params = [id: id]
    get("/asset/:id/interday", opts: [path_params: params], query: [size: size])
  end
end
