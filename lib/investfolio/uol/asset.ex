defmodule Investfolio.Uol.Asset do
  alias __MODULE__
  alias Investfolio.Assets

  defguard can_be_asset?(asset)
           when is_map(asset) and
                  :erlang.is_map_key(:idt, asset) and
                  :erlang.is_map_key(:code, asset) and
                  :erlang.is_map_key(:name, asset) and
                  :erlang.is_map_key(:companyName, asset) and
                  :erlang.is_map_key(:companyAbvName, asset)

  @derive Jason.Encoder
  defstruct idt: nil, code: nil, name: nil, companyName: nil, companyAbvName: nil

  @type t(idt, code, name, companyName, companyAbvName) :: %Asset{
          idt: idt,
          code: code,
          name: name,
          companyName: companyName,
          companyAbvName: companyAbvName
        }

  @typedoc """
      Represents Investfolio.Uol.Asset struct
  """
  @type t :: %Asset{
          idt: integer,
          code: String.t(),
          name: String.t(),
          companyName: String.t(),
          companyAbvName: String.t()
        }

  @spec build(%{
          code: String.t(),
          companyAbvName: String.t(),
          companyName: String.t(),
          idt: integer,
          name: String.t()
        }) :: Asset.t()
  def build(%{
        idt: idt,
        code: code,
        name: name,
        companyName: companyName,
        companyAbvName: companyAbvName
      }) do
    %Asset{
      idt: idt,
      code: code,
      name: name,
      companyName: companyName,
      companyAbvName: companyAbvName
    }
  end

  @spec prepare_to_schema(Asset.t()) :: map
  def prepare_to_schema(asset) do
    code = Assets.parse_code(asset.code)

    %{
      external_id: to_string(asset.idt),
      code: code,
      name: String.trim(asset.name),
      company_name: String.trim(asset.companyName),
      company_abv_name: String.trim(asset.companyAbvName),
      type: Assets.discover_asset_type(code, asset.name)
    }
  end
end
