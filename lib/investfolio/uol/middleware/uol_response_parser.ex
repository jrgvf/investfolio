defmodule Investfolio.Uol.Middleware.UolResponseParser do
  @behaviour Tesla.Middleware

  alias Investfolio.Uol.{Asset, QuoteInfo}
  import Asset, only: [can_be_asset?: 1]
  import QuoteInfo, only: [can_be_quote_info?: 1]

  @impl Tesla.Middleware
  def call(env, next, _options) do
    env
    |> Tesla.run(next)
    |> parse_response()
  end

  @errors %{
    "FSW-0001" => "Este período não é válido.",
    "FSW-0002" => "Este período não é válido; insira uma data inicial menor que a data final.",
    "FSW-0003" => "A data inicial não é válida; insira uma data menor que a data de hoje.",
    "FSW-0004" => "A data final não é válida; insira uma data menor ou igual a data atual.",
    "FSW-0101" => "Parâmetro size menor que 1",
    "FSW-0102" => "Parâmetro page menor que 1",
    "FSW-0103" => "Parâmetro fields inválido",
    "FSW-0104" => "Parâmetro idt menor que 1",
    "FSW-0201" => "Parâmetro inválido",
    "FSW-0202" => "Campo IDT inválido",
    "FSW-0401" => "Não há informação disponível para esta ação/índice.",
    "FSW-0402" => "Campo target inválido",
    "FSW-0404" => "URL não encontrada",
    "FSW-0500" => "Internal Server Error",
    "FSW-0400" => "Bad Request"
  }

  defp parse_response({:ok, %{status: status, body: body}}) when status in 200..299 do
    case body do
      %{data: data} -> {:ok, parse_data(data)}
      %{error: error_code} -> {:warning, Map.get(@errors, error_code, "Unknown Error")}
      _ -> {:warning, body}
    end
  end

  defp parse_response({:ok, %{status: status} = response}) when status in 300..499,
    do: {:warning, response}

  defp parse_response({:ok, response}),
    do: {:error, response}

  defp parse_response(error),
    do: error

  defp parse_data(data),
    do: Enum.map(data, &parse_entry/1)

  defp parse_entry(entry) when can_be_asset?(entry),
    do: Asset.build(entry)

  defp parse_entry(entry) when can_be_quote_info?(entry),
    do: QuoteInfo.build(entry)
end
