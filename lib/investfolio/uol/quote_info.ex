defmodule Investfolio.Uol.QuoteInfo do
  alias __MODULE__

  defguard can_be_quote_info?(intraday_info)
           when is_map(intraday_info) and
                  :erlang.is_map_key(:date, intraday_info) and
                  :erlang.is_map_key(:high, intraday_info) and
                  :erlang.is_map_key(:low, intraday_info) and
                  :erlang.is_map_key(:price, intraday_info) and
                  :erlang.is_map_key(:var, intraday_info) and
                  :erlang.is_map_key(:varpct, intraday_info) and
                  :erlang.is_map_key(:vol, intraday_info)

  defstruct date: nil, high: nil, low: nil, price: nil, var: nil, varpct: nil, vol: nil

  @type t(date, high, low, price, var, varpct, vol) :: %QuoteInfo{
          date: date,
          high: high,
          low: low,
          price: price,
          var: var,
          varpct: varpct,
          vol: vol
        }

  @typedoc """
      Represents Investfolio.Uol.QuoteInfo struct
  """
  @type t :: %QuoteInfo{
          date: DateTime.t(),
          high: float,
          low: float,
          price: float,
          var: float,
          varpct: float,
          vol: float
        }

  @spec build(%{
          date: pos_integer,
          high: float,
          low: float,
          price: float,
          var: float,
          varpct: float,
          vol: float
        }) ::
          Investfolio.Uol.QuoteInfo.t()
  def build(%{
        date: date,
        high: high,
        low: low,
        price: price,
        var: var,
        varpct: varpct,
        vol: vol
      }) do
    %QuoteInfo{
      date: parse_unix_to_datetime(date),
      high: high,
      low: low,
      price: price,
      var: var,
      varpct: varpct,
      vol: vol
    }
  end

  defp parse_unix_to_datetime(date) do
    date
    |> div(1000)
    |> DateTime.from_unix!()
  end
end
