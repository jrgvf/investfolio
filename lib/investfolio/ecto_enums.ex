import EctoEnum

defenum(Investfolio.Portfolios.StatusEnum, :portfolios_status, [
  :active,
  :inactive,
  :archived
])

defenum(Investfolio.Portfolios.RoleEnum, :portfolios_role, [
  :owner,
  :editor,
  :reader
])

defenum(Investfolio.Assets.TypeEnum, :assets_type, [
  :stock,
  :etf,
  :fii,
  :fip,
  :fia
])

defenum(Investfolio.Movements.TypeEnum, :movements_type, [
  :purchase,
  :sale
])

defenum(Investfolio.Dividends.TypeEnum, :dividends_type, [
  :dividends,
  :income,
  :interest_on_equity,
  :bonus
])
