defmodule Investfolio.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  alias Investfolio.RabbitMQ
  alias Investfolio.Jobs.UolAssetsFetcher

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Start Hackney Pool for Investfolio.Clients.Uol
      :hackney_pool.child_spec(:uol, timeout: 15000, max_connections: 100),
      # Start the Ecto repository
      Investfolio.Repo,
      # To sweep expired tokens from your db
      Guardian.DB.Token.SweeperServer,
      # Start the endpoint when the application starts
      InvestfolioWeb.Endpoint,
      # Start Absinthe.Subscription
      {Absinthe.Subscription, [InvestfolioWeb.Endpoint]},
      # Start RabbitMQ.Manager
      %{
        id: RabbitMQ.Manager,
        start: {RabbitMQ.Manager, :start_link, []}
      },
      # Start UolAssetsFetcher
      %{
        id: UolAssetsFetcher,
        start: {UolAssetsFetcher, :start_link, []},
        restart: :transient
      },
      # Start RabbitMQ.Consumers.UolAssetsConsumer
      RabbitMQ.Consumers.UolAssetsConsumer,
      # Start RabbitMQ.Consumers.UpsertAssetsConsumer
      RabbitMQ.Consumers.UpsertAssetsConsumer
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Investfolio.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    InvestfolioWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
