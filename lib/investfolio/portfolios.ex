defmodule Investfolio.Portfolios do
  @moduledoc """
  The Portfolios context.
  """

  import Ecto.Query, warn: false
  alias Investfolio.Repo

  alias Investfolio.Users.User
  alias Investfolio.Portfolios.{Portfolio, PortfolioUser, Movement, Dividend}

  @doc """
  Returns the list of portfolio users.

  ## Examples

      iex> list_portfolio_users()
      [%{}, ...]

  """
  def list_portfolio_users(%Portfolio{} = portfolio) do
    Repo.all(
      from p in PortfolioUser,
        where: p.portfolio_id == ^portfolio.id,
        join: u in User,
        on: u.id == p.user_id,
        select: %{
          id: p.id,
          role: p.role,
          user_id: u.id,
          user_name: u.name,
          user_email: u.email
        }
    )
  end

  @doc """
  Gets a single portfolio bu user.

  ## Examples

      iex> get_portfolio("508038e7-44b6-4ba3-89e3-ed574f7acbc6", "81a02831-c00e-4be8-9fbf-cfbf3b9e58fd")
      %Portfolio{}

      iex> get_portfolio("508038e7-44b6-4ba3-89e3-ed574f7acbc6", "81a02831-c00e-4be8-9fbf-cfbf3b9e58fg")
      nil

  """
  def get_portfolio(user_id, id, action \\ :read)

  def get_portfolio(user_id, id, action) when action in [:create, :edit, :remove] do
    Repo.one(
      from pu in PortfolioUser,
        where:
          pu.portfolio_id == ^id and pu.user_id == ^user_id and pu.role in ^[:owner, :editor],
        join: p in Portfolio,
        on: p.id == pu.portfolio_id,
        select: p
    )
  end

  def get_portfolio(user_id, id, action) when action == :read do
    Repo.one(
      from pu in PortfolioUser,
        where: pu.portfolio_id == ^id and pu.user_id == ^user_id,
        join: p in Portfolio,
        on: p.id == pu.portfolio_id,
        select: p
    )
  end

  @doc """
  Gets a single portfolio.

  Raises `Ecto.NoResultsError` if the Portfolio does not exist.

  ## Examples

      iex> get_portfolio!("81a02831-c00e-4be8-9fbf-cfbf3b9e58fd")
      %Portfolio{}

      iex> get_portfolio!("81a02831-c00e-4be8-9fbf-cfbf3b9e58fg")
      ** (Ecto.NoResultsError)

  """
  def get_portfolio!(id), do: Repo.get!(Portfolio, id)

  @doc """
  Creates a portfolio.

  ## Examples

      iex> create_portfolio(user, %{field: value})
      {:ok, %Portfolio{}}

      iex> create_portfolio(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_portfolio(%User{} = user, attrs \\ %{}) do
    Ecto.Multi.new()
    |> Ecto.Multi.insert(:portfolio, Portfolio.changeset(%Portfolio{}, attrs))
    |> Ecto.Multi.insert(:user_portfolio, fn %{portfolio: portfolio} ->
      PortfolioUser.changeset(%PortfolioUser{}, %{
        user_id: user.id,
        portfolio_id: portfolio.id
      })
    end)
    |> Repo.transaction()
    |> case do
      {:ok, %{portfolio: portfolio}} ->
        {:ok, portfolio}

      {:error, :portfolio, changeset, _} ->
        {:error, changeset}

      {:error, :user_portfolio, changeset, _} ->
        {:error, changeset}
    end
  end

  @doc """
  Updates a portfolio.

  ## Examples

      iex> update_portfolio(portfolio, %{field: new_value})
      {:ok, %Portfolio{}}

      iex> update_portfolio(portfolio, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_portfolio(%Portfolio{} = portfolio, attrs) do
    portfolio
    |> Portfolio.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a portfolio.

  ## Examples

      iex> delete_portfolio(portfolio)
      {:ok, %Portfolio{}}

      iex> delete_portfolio(portfolio)
      {:error, %Ecto.Changeset{}}

  """
  def delete_portfolio(%Portfolio{} = portfolio) do
    Repo.delete(portfolio)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking portfolio changes.

  ## Examples

      iex> change_portfolio(portfolio)
      %Ecto.Changeset{source: %Portfolio{}}

  """
  def change_portfolio(%Portfolio{} = portfolio) do
    Portfolio.changeset(portfolio, %{})
  end

  @doc """
  Returns the list of portfolio movements.

  ## Examples

      iex> list_movements(portfolio)
      [%Movement{}, ...]

  """
  def list_movements(%Portfolio{} = portfolio) do
    Repo.all(Ecto.assoc(portfolio, :movements))
  end

  def get_movement(portfolio_id, id),
    do: Repo.get_by(Movement, id: id, portfolio_id: portfolio_id)

  @doc """
  Gets a single movement.

  Raises `Ecto.NoResultsError` if the Movement does not exist.

  ## Examples

      iex> get_movement!(123)
      %Movement{}

      iex> get_movement!(456)
      ** (Ecto.NoResultsError)

  """
  def get_movement!(id), do: Repo.get!(Movement, id)

  @doc """
  Creates a movement.

  ## Examples

      iex> create_movement(%{field: value})
      {:ok, %Movement{}}

      iex> create_movement(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_movement(attrs \\ %{}) do
    %Movement{}
    |> Movement.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a movement.

  ## Examples

      iex> update_movement(movement, %{field: new_value})
      {:ok, %Movement{}}

      iex> update_movement(movement, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_movement(%Movement{} = movement, attrs) do
    movement
    |> Movement.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a movement.

  ## Examples

      iex> delete_movement(movement)
      {:ok, %Movement{}}

      iex> delete_movement(movement)
      {:error, %Ecto.Changeset{}}

  """
  def delete_movement(%Movement{} = movement) do
    Repo.delete(movement)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking movement changes.

  ## Examples

      iex> change_movement(movement)
      %Ecto.Changeset{source: %Movement{}}

  """
  def change_movement(%Movement{} = movement) do
    Movement.changeset(movement, %{})
  end

  @doc """
  Returns the list of portfolio dividends.

  ## Examples

      iex> list_dividends(portfolio)
      [%Dividend{}, ...]

  """
  def list_dividends(%Portfolio{} = portfolio) do
    Repo.all(Ecto.assoc(portfolio, :dividends))
  end

  def get_dividend(portfolio_id, id),
    do: Repo.get_by(Dividend, id: id, portfolio_id: portfolio_id)

  @doc """
  Gets a single dividend.

  Raises `Ecto.NoResultsError` if the Dividend does not exist.

  ## Examples

      iex> get_dividend!(123)
      %Dividend{}

      iex> get_dividend!(456)
      ** (Ecto.NoResultsError)

  """
  def get_dividend!(id), do: Repo.get!(Dividend, id)

  @doc """
  Creates a dividend.

  ## Examples

      iex> create_dividend(%{field: value})
      {:ok, %Dividend{}}

      iex> create_dividend(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_dividend(attrs \\ %{}) do
    %Dividend{}
    |> Dividend.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a dividend.

  ## Examples

      iex> update_dividend(dividend, %{field: new_value})
      {:ok, %Dividend{}}

      iex> update_dividend(dividend, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_dividend(%Dividend{} = dividend, attrs) do
    dividend
    |> Dividend.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a dividend.

  ## Examples

      iex> delete_dividend(dividend)
      {:ok, %Dividend{}}

      iex> delete_dividend(dividend)
      {:error, %Ecto.Changeset{}}

  """
  def delete_dividend(%Dividend{} = dividend) do
    Repo.delete(dividend)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking dividend changes.

  ## Examples

      iex> change_dividend(dividend)
      %Ecto.Changeset{source: %Dividend{}}

  """
  def change_dividend(%Dividend{} = dividend) do
    Dividend.changeset(dividend, %{})
  end
end
