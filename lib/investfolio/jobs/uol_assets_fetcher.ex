defmodule Investfolio.Jobs.UolAssetsFetcher do
  use GenServer

  require Logger

  alias Investfolio.RabbitMQ
  alias Investfolio.Uol.{Asset, Client}

  def start_link do
    GenServer.start(__MODULE__, %{status: :idle}, name: __MODULE__)
  end

  @impl true
  def init(state),
    do: {:ok, state}

  def start_generating,
    do: GenServer.call(__MODULE__, :start_generating)

  @impl true
  def handle_info(:generate_uol_assets, state) do
    Logger.info("#{__MODULE__}: starting")

    with {:ok, uol_assets} <- Client.assets_list() do
      total_assets = Enum.count(uol_assets)
      Logger.info("#{__MODULE__}: loaded assets - Total: #{total_assets}")

      encoded_uol_assets =
        uol_assets
        |> Enum.map(&encode_uol_asset/1)
        |> Enum.filter(& &1)

      total_valid_assets = Enum.count(encoded_uol_assets)

      Logger.info(
        "#{__MODULE__}: encoded assets - Valids: #{total_valid_assets}, Invalids: #{
          total_assets - total_valid_assets
        }"
      )

      Logger.info("#{__MODULE__}: queuing assets")
      queue = RabbitMQ.Manager.uol_assets_queue_name()
      Enum.each(encoded_uol_assets, &RabbitMQ.Manager.publish(queue, &1))

      Logger.info("#{__MODULE__}: queued assets")
    end

    Logger.info("#{__MODULE__}: finished")
    {:noreply, %{state | status: :idle}}
  end

  @impl true
  def handle_call(:start_generating, _from, state) do
    send(self(), :generate_uol_assets)
    {:reply, :ok, %{state | status: :running}}
  end

  defp encode_uol_asset(%Asset{} = uol_asset) do
    with {:ok, encoded_asset} <- Jason.encode(uol_asset) do
      encoded_asset
    else
      _ -> nil
    end
  end
end
