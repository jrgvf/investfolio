defmodule Investfolio.Assets do
  @moduledoc """
  The Assets context.
  """

  import Ecto.Query, warn: false
  alias Investfolio.Repo

  alias Investfolio.Assets.{Asset, KnownAsset}
  alias Investfolio.Uol.Client

  def search(term) do
    Repo.all(
      from q in Asset,
        where:
          ilike(q.code, ^"%#{term}%") or
            fragment("similarity(?, ?) >= ?", q.name, ^term, 0.2) or
            fragment("similarity(?, ?) >= ?", q.company_name, ^term, 0.2),
        order_by: fragment("similarity(?, ?) DESC", q.code, ^term)
    )
  end

  @doc """
  Returns the list of assets.

  ## Examples

      iex> list_assets()
      [%Asset{}, ...]

  """
  def list_assets(args \\ []) do
    args
    |> assets_query()
    |> Repo.all()
  end

  @doc """
  Gets a single asset by code.

  ## Examples

      iex> get_by_code("MXRF11")
      %Asset{}

      iex> get_by_code("UKNW3")
      ** nil

  """
  def get_by_code(code),
    do: Repo.get_by(Asset, code: code)

  @doc """
  Gets a single asset.

  Raises `Ecto.NoResultsError` if the Asset does not exist.

  ## Examples

      iex> get_asset!(123)
      %Asset{}

      iex> get_asset!(456)
      ** (Ecto.NoResultsError)

  """
  def get_asset!(id), do: Repo.get!(Asset, id)

  @doc """
  Creates a asset.

  ## Examples

      iex> create_asset(%{field: value})
      {:ok, %Asset{}}

      iex> create_asset(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_asset(attrs, options \\ []) do
    %Asset{}
    |> Asset.changeset(attrs)
    |> Repo.insert(options)
  end

  @doc """
  Updates a asset.

  ## Examples

      iex> update_asset(asset, %{field: new_value})
      {:ok, %Asset{}}

      iex> update_asset(asset, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_asset(%Asset{} = asset, attrs) do
    asset
    |> Asset.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a asset.

  ## Examples

      iex> delete_asset(asset)
      {:ok, %Asset{}}

      iex> delete_asset(asset)
      {:error, %Ecto.Changeset{}}

  """
  def delete_asset(%Asset{} = asset) do
    Repo.delete(asset)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking asset changes.

  ## Examples

      iex> change_asset(asset)
      %Ecto.Changeset{source: %Asset{}}

  """
  def change_asset(%Asset{} = asset) do
    Asset.changeset(asset, %{})
  end

  @doc """
  Returns a asset code.

  ## Examples

      iex> parse_code("MXRF11.SA")
      "MXRF11"

      iex> parse_code("MGLU3")
      "MGLU3"

  """
  @south_america_suffix ".SA"
  def parse_code(code),
    do: String.replace_suffix(code, @south_america_suffix, "")

  @doc """
  Returns a Investfolio.Assets.TypeEnum value.

  ## Examples

      iex> discover_asset_type("MXRF11.SA", "FII MAXI REN")
      :fii

      iex> discover_asset_type("MGLU3", "MAG LUIZA ON")
      :stock

  """
  def discover_asset_type(code, name) do
    with code <- String.replace_suffix(code, @south_america_suffix, ""),
         {:ok, type} <- KnownAsset.find_type(code) do
      type
    else
      _ -> discover_type_by_name(name)
    end
  end

  def fetch_quote(%Asset{} = asset) do
    fetch_intraday(asset, 1)
  end

  def fetch_intraday(%Asset{} = asset, size \\ nil) do
    Client.asset_intraday(asset.external_id, size)
  end

  def fetch_interday(%Asset{} = asset, size \\ nil) do
    Client.asset_interday(asset.external_id, size)
  end

  @discoverable_types ~w(fii fip fia etf)a
  @default_type :stock
  defp discover_type_by_name(name) do
    @discoverable_types
    |> Enum.find(@default_type, &check_type(name, &1))
  end

  defp check_type(name, type) do
    regex = build_type_regex(type)
    String.match?(name, regex)
  end

  defp build_type_regex(type) when is_atom(type),
    do: build_type_regex("#{type}")

  defp build_type_regex(type) when is_bitstring(type) do
    type = String.upcase(type)
    Regex.compile!("#{type}\\s+|\\s+#{type}")
  end

  defp assets_query(args) do
    Enum.reduce(args, Asset, fn
      {:order, value}, query ->
        [attribute, order] =
          value
          |> to_string()
          |> String.split("_")
          |> Enum.map(&String.to_atom/1)

        order_by(query, {^order, ^attribute})

      {:filter, filter}, query ->
        filter_with(query, filter)
    end)
  end

  defp filter_with(query, filter) do
    Enum.reduce(filter, query, fn
      {:name, name}, query ->
        from q in query,
          where: fragment("similarity(?, ?) >= ?", q.name, ^name, 0.2),
          order_by: fragment("similarity(?, ?) DESC", q.name, ^name)

      {:code, code}, query ->
        from q in query,
          where: ilike(q.code, ^"%#{code}%")

      {:company_name, company_name}, query ->
        from q in query,
          where: fragment("similarity(?, ?) >= ?", q.company_name, ^company_name, 0.2),
          order_by: fragment("similarity(?, ?) DESC", q.company_name, ^company_name)

      {:type, type}, query ->
        from q in query,
          where: q.type == ^"#{type}"
    end)
  end
end
