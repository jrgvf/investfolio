defmodule InvestfolioWeb.GraphQL.Mutations.Users do
  use Absinthe.Schema.Notation
  alias InvestfolioWeb.GraphQL.Resolvers

  object :users_mutations do
    field :create_user, :create_user_result do
      arg(:input, non_null(:create_user_input))
      resolve(&Resolvers.Users.create/3)
    end

    field :login, :session do
      arg(:email, non_null(:string))
      arg(:password, non_null(:string))
      resolve(&Resolvers.Users.login/3)
    end

    field :refresh, :session do
      arg(:refresh_token, non_null(:string))
      arg(:access_token, non_null(:string))
      resolve(&Resolvers.Users.refresh/3)
    end

    field :revoke, :revoke_result do
      arg(:refresh_token, non_null(:string))
      arg(:access_token, non_null(:string))
      resolve(&Resolvers.Users.revoke/3)
    end
  end
end
