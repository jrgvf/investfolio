defmodule InvestfolioWeb.GraphQL.Mutations.Portfolios do
  use Absinthe.Schema.Notation
  alias InvestfolioWeb.GraphQL.{Middlewares, Resolvers}

  object :portfolios_mutations do
    field :create_portfolio, :create_portfolio_result do
      arg(:input, non_null(:create_portfolio_input))

      middleware(Middlewares.Authorize)
      resolve(&Resolvers.Portfolios.create/3)
    end

    field :update_portfolio, :update_portfolio_result do
      arg(:input, non_null(:update_portfolio_input))

      middleware(Middlewares.Authorize)
      middleware(Middlewares.CheckPermission, subject: :portfolio, action: :edit)

      resolve(&Resolvers.Portfolios.update/3)
    end

    field :remove_portfolio, :remove_portfolio_result do
      arg(:input, non_null(:remove_portfolio_input))

      middleware(Middlewares.Authorize)
      middleware(Middlewares.CheckPermission, subject: :portfolio, action: :remove)

      resolve(&Resolvers.Portfolios.remove/3)
    end
  end
end
