defmodule InvestfolioWeb.GraphQL.Mutations.Movements do
  use Absinthe.Schema.Notation
  alias InvestfolioWeb.GraphQL.{Middlewares, Resolvers}

  object :movements_mutations do
    field :create_movement, :create_movement_result do
      arg(:input, non_null(:create_movement_input))

      middleware(Middlewares.Authorize)
      middleware(Middlewares.CheckPermission, subject: :portfolio, action: :create)

      resolve(&Resolvers.Movements.create/3)
    end

    field :update_movement, :update_movement_result do
      arg(:input, non_null(:update_movement_input))

      middleware(Middlewares.Authorize)
      middleware(Middlewares.CheckPermission, subject: :movement, action: :edit)

      resolve(&Resolvers.Movements.update/3)
    end

    field :remove_movement, :remove_movement_result do
      arg(:input, non_null(:remove_movement_input))

      middleware(Middlewares.Authorize)
      middleware(Middlewares.CheckPermission, subject: :movement, action: :remove)

      resolve(&Resolvers.Movements.remove/3)
    end
  end
end
