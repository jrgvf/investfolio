defmodule InvestfolioWeb.GraphQL.Mutations.Dividends do
  use Absinthe.Schema.Notation
  alias InvestfolioWeb.GraphQL.{Middlewares, Resolvers}

  object :dividends_mutations do
    field :create_dividend, :create_dividend_result do
      arg(:input, non_null(:create_dividend_input))

      middleware(Middlewares.Authorize)
      middleware(Middlewares.CheckPermission, subject: :portfolio, action: :create)

      resolve(&Resolvers.Dividends.create/3)
    end

    field :update_dividend, :update_dividend_result do
      arg(:input, non_null(:update_dividend_input))

      middleware(Middlewares.Authorize)
      middleware(Middlewares.CheckPermission, subject: :dividend, action: :edit)

      resolve(&Resolvers.Dividends.update/3)
    end

    field :remove_dividend, :remove_dividend_result do
      arg(:input, non_null(:remove_dividend_input))

      middleware(Middlewares.Authorize)
      middleware(Middlewares.CheckPermission, subject: :dividend, action: :remove)

      resolve(&Resolvers.Dividends.remove/3)
    end
  end
end
