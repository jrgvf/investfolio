defmodule InvestfolioWeb.GraphQL.Resolvers.Assets do
  alias Investfolio.Assets

  def list_assets(_, args, _),
    do: {:ok, Assets.list_assets(args)}

  def search(_, %{matching: term}, _),
    do: {:ok, Assets.search(term)}

  def asset_by_code(%{asset_code: asset_code}, _, _),
    do: {:ok, Assets.get_by_code(asset_code)}
end
