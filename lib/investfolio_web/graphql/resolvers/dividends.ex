defmodule InvestfolioWeb.GraphQL.Resolvers.Dividends do
  alias Investfolio.Portfolios
  alias Investfolio.Assets

  def create(_, %{input: %{asset_code: asset_code} = params}, _) do
    with %Assets.Asset{id: asset_id} <- Assets.get_by_code(asset_code),
         params <- Map.put(params, :asset_id, asset_id),
         {:ok, dividend} <- Portfolios.create_dividend(params) do
      {:ok, %{dividend: dividend, success: true}}
    else
      nil -> {:error, "Asset not found"}
      error -> error
    end
  end

  def update(_, %{input: %{asset_code: asset_code} = params}, %{
        context: %{dividend: dividend}
      }) do
    with %Assets.Asset{id: asset_id} <- Assets.get_by_code(asset_code),
         params <- Map.put(params, :asset_id, asset_id),
         {:ok, dividend} <- Portfolios.update_dividend(dividend, params) do
      {:ok, %{dividend: dividend, success: true}}
    else
      nil -> {:error, "Asset not found"}
      error -> error
    end
  end

  def remove(_, _, %{context: %{dividend: dividend}}) do
    with {:ok, dividend} <- Portfolios.delete_dividend(dividend) do
      {:ok, %{dividend: dividend, success: true}}
    end
  end

  def list(portfolio, _, _),
    do: {:ok, Portfolios.list_dividends(portfolio)}
end
