defmodule InvestfolioWeb.GraphQL.Resolvers.Portfolios do
  alias Investfolio.Portfolios

  def create(_, %{input: params}, %{context: %{current_user: current_user}}) do
    with {:ok, portfolio} <- Portfolios.create_portfolio(current_user, params) do
      {:ok, %{portfolio: portfolio, success: true}}
    end
  end

  def update(_, %{input: params}, %{context: %{portfolio: portfolio}}) do
    with {:ok, portfolio} <- Portfolios.update_portfolio(portfolio, params) do
      {:ok, %{portfolio: portfolio, success: true}}
    end
  end

  def remove(_, _, %{context: %{portfolio: portfolio}}) do
    with {:ok, portfolio} <- Portfolios.delete_portfolio(portfolio) do
      {:ok, %{portfolio: portfolio, success: true}}
    end
  end

  def users(portfolio, _, _),
    do: {:ok, Portfolios.list_portfolio_users(portfolio)}

  def find(viewer, %{id: id}, _),
    do: {:ok, Portfolios.get_portfolio(viewer.id, id)}
end
