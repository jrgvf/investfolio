defmodule InvestfolioWeb.GraphQL.Resolvers.Users do
  alias Investfolio.Users

  def create(_, %{input: params}, _) do
    with {:ok, user} <- Users.create_user(params) do
      {:ok, %{user: user, success: true}}
    end
  end

  def login(_, %{email: email, password: password}, _) do
    with {:ok, user} <- Users.authenticate(email, password),
         {:ok, access_token, _} <- Investfolio.Auth.encode_and_sign(user),
         {:ok, _, {refresh_token, _}} <-
           Investfolio.Auth.exchange(access_token, "access", "refresh", ttl: {4, :weeks}) do
      {:ok, %{access_token: access_token, refresh_token: refresh_token}}
    else
      _ -> {:error, "incorrect email or password"}
    end
  end

  def refresh(_, %{refresh_token: refresh_token, access_token: access_token}, _)
      when is_nil(access_token) or access_token == "" do
    with {:ok, _} <- Investfolio.Auth.decode_and_verify(refresh_token, %{typ: "refresh"}),
         {:ok, _, {new_refresh_token, _}} <-
           Investfolio.Auth.refresh(refresh_token, ttl: {4, :weeks}),
         {:ok, _, {new_access_token, _}} <-
           Investfolio.Auth.exchange(new_refresh_token, "refresh", "access") do
      {:ok, %{access_token: new_access_token, refresh_token: new_refresh_token}}
    else
      _ -> {:error, "invalid or expired refresh token"}
    end
  end

  def refresh(_, %{refresh_token: refresh_token, access_token: access_token}, _) do
    with {:ok, _} <- Investfolio.Auth.decode_and_verify(refresh_token, %{typ: "refresh"}),
         _ <- Investfolio.Auth.revoke(access_token),
         {:ok, _, {new_refresh_token, _}} <-
           Investfolio.Auth.refresh(refresh_token, ttl: {4, :weeks}),
         {:ok, _, {new_access_token, _}} <-
           Investfolio.Auth.exchange(new_refresh_token, "refresh", "access") do
      {:ok, %{access_token: new_access_token, refresh_token: new_refresh_token}}
    else
      _ -> {:error, "invalid or expired refresh token"}
    end
  end

  def revoke(_, %{refresh_token: refresh_token, access_token: access_token}, _) do
    with _ <- Investfolio.Auth.revoke(access_token),
         _ <- Investfolio.Auth.revoke(refresh_token) do
      {:ok, %{result: "tokens revoked"}}
    end
  end

  def viewer(_, _, %{context: %{current_user: current_user}}) do
    {:ok, current_user}
  end

  def list_portfolios(viewer, _, _) do
    {:ok, Users.list_portfolios(viewer)}
  end
end
