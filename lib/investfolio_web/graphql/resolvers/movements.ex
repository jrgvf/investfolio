defmodule InvestfolioWeb.GraphQL.Resolvers.Movements do
  alias Investfolio.Portfolios
  alias Investfolio.Assets

  def create(_, %{input: %{asset_code: asset_code} = params}, _) do
    with %Assets.Asset{id: asset_id} <- Assets.get_by_code(asset_code),
         params <- Map.put(params, :asset_id, asset_id),
         {:ok, movement} <- Portfolios.create_movement(params) do
      {:ok, %{movement: movement, success: true}}
    else
      nil -> {:error, "Asset not found"}
      error -> error
    end
  end

  def update(_, %{input: %{asset_code: asset_code} = params}, %{
        context: %{movement: movement}
      }) do
    with %Assets.Asset{id: asset_id} <- Assets.get_by_code(asset_code),
         params <- Map.put(params, :asset_id, asset_id),
         {:ok, movement} <- Portfolios.update_movement(movement, params) do
      {:ok, %{movement: movement, success: true}}
    else
      nil -> {:error, "Asset not found"}
      error -> error
    end
  end

  def remove(_, _, %{context: %{movement: movement}}) do
    with {:ok, movement} <- Portfolios.delete_movement(movement) do
      {:ok, %{movement: movement, success: true}}
    end
  end

  def list(portfolio, _, _),
    do: {:ok, Portfolios.list_movements(portfolio)}
end
