defmodule InvestfolioWeb.GraphQL.Types.Utils do
  use Absinthe.Schema.Notation

  @desc "An error encountered trying to persist input"
  object :input_error do
    field :key, non_null(:string)
    field :message, non_null(:string)
  end

  @desc "Session tokens"
  object :session do
    field :access_token, non_null(:string)
    field :refresh_token, non_null(:string)
  end

  @desc "Revoke session tokens result"
  object :revoke_result do
    field :result, non_null(:string)
  end

  enum :assets_sort_order do
    value(:name_asc)
    value(:name_desc)
    value(:code_asc)
    value(:code_desc)
  end
end
