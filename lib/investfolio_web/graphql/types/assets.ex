defmodule InvestfolioWeb.GraphQL.Types.Assets do
  use Absinthe.Schema.Notation

  enum :assets_type do
    value(:stock)
    value(:etf)
    value(:fii)
    value(:fia)
    value(:fip)
  end

  object :asset do
    field :code, non_null(:string)
    field :name, non_null(:string)
    field :company_name, non_null(:string)
    field :type, non_null(:assets_type)
  end

  @desc "Filtering options for the assets list"
  input_object :assets_filter do
    @desc "Similarity by name - It's disable the order filter"
    field :name, :string
    @desc "Matching a code"
    field :code, :string
    @desc "Similarity by companyName - It's disable the order filter"
    field :company_name, :string
    @desc "Filtering a type"
    field :type, :assets_type
  end
end
