defmodule InvestfolioWeb.GraphQL.Types.Users do
  use Absinthe.Schema.Notation
  alias InvestfolioWeb.GraphQL.Resolvers

  input_object :create_user_input do
    field :name, non_null(:string)
    field :email, non_null(:string)
    field :email_confirmation, non_null(:string)
    field :password, non_null(:string)
    field :password_confirmation, non_null(:string)
  end

  object :create_user_result do
    field :success, non_null(:boolean)
    field :user, :user
    field :errors, list_of(:input_error)
  end

  object :user do
    field :id, non_null(:uuid4)
    field :name, non_null(:string)
    field :email, non_null(:string)

    field :portfolios, non_null(list_of(:portfolio)), resolve: &Resolvers.Users.list_portfolios/3

    field :portfolio, :portfolio do
      arg(:id, non_null(:uuid4))
      resolve(&Resolvers.Portfolios.find/3)
    end
  end
end
