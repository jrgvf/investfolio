defmodule InvestfolioWeb.GraphQL.Types.Portfolios do
  use Absinthe.Schema.Notation
  alias InvestfolioWeb.GraphQL.Resolvers

  enum :portfolios_status do
    value(:active)
    value(:inactive)
    value(:archived)
  end

  enum :portfolios_role do
    value(:owner)
    value(:editor)
    value(:reader)
  end

  input_object :create_portfolio_input do
    field :name, non_null(:string)
    field :description, :string
    field :status, :portfolios_status
  end

  object :create_portfolio_result do
    field :success, non_null(:boolean)
    field :portfolio, :portfolio
    field :errors, list_of(:input_error)
  end

  input_object :update_portfolio_input do
    field :portfolio_id, non_null(:uuid4)
    import_fields(:create_portfolio_input)
  end

  object :update_portfolio_result do
    import_fields(:create_portfolio_result)
  end

  input_object :remove_portfolio_input do
    field :portfolio_id, non_null(:uuid4)
  end

  object :remove_portfolio_result do
    field :success, non_null(:boolean)
    field :portfolio, :portfolio
  end

  object :portfolio do
    field :id, non_null(:uuid4)
    field :name, non_null(:string)
    field :description, :string
    field :status, :portfolios_status
    field :portfolio_users, list_of(:portfolio_user), resolve: &Resolvers.Portfolios.users/3
    field :movements, list_of(:movement), resolve: &Resolvers.Movements.list/3
    field :dividends, list_of(:dividend), resolve: &Resolvers.Dividends.list/3
  end

  object :portfolio_user do
    field :id, non_null(:uuid4)
    field :user_id, non_null(:uuid4)
    field :user_name, non_null(:string)
    field :user_email, non_null(:string)
    field :role, non_null(:portfolios_role)
  end
end
