defmodule InvestfolioWeb.GraphQL.Types.Dividends do
  use Absinthe.Schema.Notation
  alias InvestfolioWeb.GraphQL.Resolvers

  enum :dividends_type do
    value(:dividends)
    value(:income)
    value(:interest_on_equity)
    value(:bonus)
  end

  object :dividend do
    field :id, non_null(:uuid4)
    field :asset_code, non_null(:string)
    field :type, non_null(:dividends_type)
    field :base_date, non_null(:date)
    field :isin, :string
    field :payment_date, non_null(:date)
    field :quantity, non_null(:integer)
    field :total_received, non_null(:decimal)
    field :value, non_null(:decimal)
    field :reference_month, non_null(:integer)
    field :reference_year, non_null(:integer)
    field :asset, :asset, resolve: &Resolvers.Assets.asset_by_code/3
  end

  input_object :create_dividend_input do
    field :portfolio_id, non_null(:uuid4)
    field :asset_code, non_null(:string)
    field :isin, :string
    field :type, non_null(:dividends_type), default_value: :income
    field :base_date, non_null(:date)
    field :payment_date, non_null(:date)
    field :quantity, non_null(:integer)
    field :value, non_null(:decimal)
  end

  object :create_dividend_result do
    field :success, non_null(:boolean)
    field :dividend, :dividend
    field :errors, list_of(:input_error)
  end

  input_object :update_dividend_input do
    field :dividend_id, non_null(:uuid4)
    import_fields(:create_dividend_input)
  end

  object :update_dividend_result do
    import_fields(:create_dividend_result)
  end

  input_object :remove_dividend_input do
    field :portfolio_id, non_null(:uuid4)
    field :dividend_id, non_null(:uuid4)
  end

  object :remove_dividend_result do
    field :success, non_null(:boolean)
    field :dividend, :dividend
  end
end
