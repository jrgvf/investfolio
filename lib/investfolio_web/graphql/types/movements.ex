defmodule InvestfolioWeb.GraphQL.Types.Movements do
  use Absinthe.Schema.Notation
  alias InvestfolioWeb.GraphQL.Resolvers

  enum :movements_type do
    value(:purchase)
    value(:sale)
  end

  object :movement do
    field :id, non_null(:uuid4)
    field :asset_code, non_null(:string)
    field :type, non_null(:movements_type)
    field :isin, :string
    field :quantity, non_null(:integer)
    field :description, :string
    field :date, non_null(:date)
    field :average_price, non_null(:decimal)
    field :operational_cost, non_null(:decimal)
    field :total_value, non_null(:decimal)
    field :asset, :asset, resolve: &Resolvers.Assets.asset_by_code/3
  end

  input_object :create_movement_input do
    field :portfolio_id, non_null(:uuid4)
    field :asset_code, non_null(:string)
    field :description, :string
    field :isin, :string
    field :type, non_null(:movements_type)
    field :quantity, non_null(:integer)
    field :date, non_null(:date)
    field :average_price, non_null(:decimal)
    field :operational_cost, non_null(:decimal)
  end

  object :create_movement_result do
    field :success, non_null(:boolean)
    field :movement, :movement
    field :errors, list_of(:input_error)
  end

  input_object :update_movement_input do
    field :movement_id, non_null(:uuid4)
    import_fields(:create_movement_input)
  end

  object :update_movement_result do
    import_fields(:create_movement_result)
  end

  input_object :remove_movement_input do
    field :portfolio_id, non_null(:uuid4)
    field :movement_id, non_null(:uuid4)
  end

  object :remove_movement_result do
    field :success, non_null(:boolean)
    field :movement, :movement
  end
end
