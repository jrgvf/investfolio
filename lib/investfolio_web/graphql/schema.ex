defmodule InvestfolioWeb.GraphQL.Schema do
  use Absinthe.Schema
  alias InvestfolioWeb.GraphQL.{Middlewares, Types, Queries, Mutations}

  def middleware(middleware, field, object) do
    middleware
    |> apply(:errors, field, object)
  end

  defp apply(middleware, :errors, _field, %{identifier: :mutation}) do
    middleware ++ [Middlewares.ChangesetErrors]
  end

  defp apply(middleware, _, _, _) do
    middleware
  end

  # Types
  import_types(Types.Assets)
  import_types(Types.Portfolios)
  import_types(Types.Movements)
  import_types(Types.Dividends)
  import_types(Types.Users)
  import_types(Types.Utils)

  # Scalar
  import_types(Types.UUID4)
  import_types(Absinthe.Type.Custom)

  # Queries
  import_types(Queries.Assets)
  import_types(Queries.Users)

  # Mutations
  import_types(Mutations.Portfolios)
  import_types(Mutations.Movements)
  import_types(Mutations.Dividends)
  import_types(Mutations.Users)

  query do
    import_fields(:assets_queries)
    import_fields(:users_queries)
  end

  mutation do
    import_fields(:users_mutations)
    import_fields(:portfolios_mutations)
    import_fields(:movements_mutations)
    import_fields(:dividends_mutations)
  end
end
