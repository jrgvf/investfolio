defmodule InvestfolioWeb.GraphQL.Queries.Assets do
  use Absinthe.Schema.Notation
  alias InvestfolioWeb.GraphQL.Resolvers

  object :assets_queries do
    field :assets, list_of(:asset) do
      arg(:order, type: :assets_sort_order, default_value: :name_asc)
      arg(:filter, :assets_filter)
      resolve(&Resolvers.Assets.list_assets/3)
    end

    field :search, list_of(:asset) do
      arg(:matching, non_null(:string))
      resolve(&Resolvers.Assets.search/3)
    end
  end
end
