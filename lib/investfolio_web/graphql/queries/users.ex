defmodule InvestfolioWeb.GraphQL.Queries.Users do
  use Absinthe.Schema.Notation
  alias InvestfolioWeb.GraphQL.{Middlewares, Resolvers}

  object :users_queries do
    field :viewer, :user do
      middleware(Middlewares.Authorize)
      resolve(&Resolvers.Users.viewer/3)
    end
  end
end
