defmodule InvestfolioWeb.GraphQL.Middlewares.Authorize do
  @behaviour Absinthe.Middleware

  alias Investfolio.Users.User

  def call(resolution, _) do
    with %{current_user: %User{}} <- resolution.context do
      resolution
    else
      _ ->
        resolution
        |> Absinthe.Resolution.put_result({:error, "unauthorized"})
    end
  end
end
