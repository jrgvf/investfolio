defmodule InvestfolioWeb.GraphQL.Middlewares.CheckPermission do
  @behaviour Absinthe.Middleware

  alias Investfolio.Users.User
  alias Investfolio.Portfolios
  alias Investfolio.Portfolios.{Portfolio, Movement, Dividend}

  def call(%{context: %{current_user: %User{id: user_id}}} = resolution,
        subject: :portfolio,
        action: action
      ) do
    with %{input: %{portfolio_id: portfolio_id}} <- resolution.arguments,
         %Portfolio{} = portfolio <- Portfolios.get_portfolio(user_id, portfolio_id, action) do
      %{resolution | context: Map.put(resolution.context, :portfolio, portfolio)}
    else
      _ ->
        resolution
        |> Absinthe.Resolution.put_result({:error, "Portfolio not found"})
    end
  end

  def call(%{context: %{current_user: %User{id: user_id}}} = resolution,
        subject: :movement,
        action: action
      ) do
    with %{input: %{portfolio_id: portfolio_id, movement_id: movement_id}} <-
           resolution.arguments,
         %Portfolio{} = portfolio <-
           Portfolios.get_portfolio(user_id, portfolio_id, action),
         %Movement{} = movement <- Portfolios.get_movement(portfolio_id, movement_id) do
      new_context =
        resolution.context
        |> Map.put(:portfolio, portfolio)
        |> Map.put(:movement, movement)

      %{resolution | context: new_context}
    else
      _ ->
        resolution
        |> Absinthe.Resolution.put_result({:error, "Movement not found"})
    end
  end

  def call(%{context: %{current_user: %User{id: user_id}}} = resolution,
        subject: :dividend,
        action: action
      ) do
    with %{input: %{portfolio_id: portfolio_id, dividend_id: dividend_id}} <-
           resolution.arguments,
         %Portfolio{} = portfolio <-
           Portfolios.get_portfolio(user_id, portfolio_id, action),
         %Dividend{} = dividend <- Portfolios.get_dividend(portfolio_id, dividend_id) do
      new_context =
        resolution.context
        |> Map.put(:portfolio, portfolio)
        |> Map.put(:dividend, dividend)

      %{resolution | context: new_context}
    else
      _ ->
        resolution
        |> Absinthe.Resolution.put_result({:error, "Dividend not found"})
    end
  end

  def call(resolution, _),
    do: resolution
end
