defmodule InvestfolioWeb.Router do
  use InvestfolioWeb, :router
  alias InvestfolioWeb.GraphQL

  pipeline :graphql do
    plug :accepts, ["json"]
    plug GraphQL.Plugs.Context
  end

  scope "/" do
    pipe_through :graphql

    forward "/api", Absinthe.Plug, schema: GraphQL.Schema

    forward "/graphiql", Absinthe.Plug.GraphiQL,
      schema: GraphQL.Schema,
      socket: InvestfolioWeb.UserSocket
  end
end
