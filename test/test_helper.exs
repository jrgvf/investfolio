ExUnit.start()
ExUnit.configure(assert_receive_timeout: 2000)
Ecto.Adapters.SQL.Sandbox.mode(Investfolio.Repo, :manual)
