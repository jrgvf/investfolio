defmodule Investfolio.Factory do
  alias Investfolio.Repo
  alias Investfolio.Users.User
  alias Investfolio.Portfolios
  alias Investfolio.Portfolios.{PortfolioUser, Movement, Dividend}
  alias Investfolio.Assets.Asset

  def create_user(password \\ "super-secret") do
    int = :erlang.unique_integer([:positive, :monotonic])

    params = %{
      name: "Person #{int}",
      email: "fake-#{int}@example.com",
      password: password
    }

    %User{}
    |> User.changeset(params)
    |> Repo.insert!()
  end

  def create_portfolio(user, status \\ :active) do
    int = :erlang.unique_integer([:positive, :monotonic])

    params = %{
      name: "Portfolio #{int}",
      description: "Test Portfolio #{int}",
      status: status
    }

    {:ok, portfolio} = Portfolios.create_portfolio(user, params)
    portfolio
  end

  def create_portfolio_user(user_id, portfolio_id, role \\ :editor) do
    params = %{
      user_id: user_id,
      portfolio_id: portfolio_id,
      role: role
    }

    %PortfolioUser{}
    |> PortfolioUser.changeset(params)
    |> Repo.insert!()
  end

  def create_asset(code \\ random_asset_code(), type \\ :stock) do
    int = :erlang.unique_integer([:positive, :monotonic])

    params = %{
      code: code,
      company_abv_name: "Fake #{code}",
      company_name: "Fake Company #{code}",
      external_id: "#{int}",
      name: "Fake #{code}",
      type: type
    }

    %Asset{}
    |> Asset.changeset(params)
    |> Repo.insert!()
  end

  def create_movement(asset, portifolio_id, type \\ :purchase) do
    params = %{
      asset_code: asset.code,
      asset_id: asset.id,
      average_price: 10.0,
      date: Date.utc_today(),
      operational_cost: 0.0,
      portfolio_id: portifolio_id,
      quantity: 10,
      type: type
    }

    %Movement{}
    |> Movement.changeset(params)
    |> Repo.insert!()
  end

  def create_dividend(asset, portifolio_id, type \\ :income) do
    params = %{
      asset_code: asset.code,
      asset_id: asset.id,
      value: 0.08,
      base_date: Date.utc_today(),
      payment_date: Date.utc_today(),
      portfolio_id: portifolio_id,
      quantity: 10,
      type: type
    }

    %Dividend{}
    |> Dividend.changeset(params)
    |> Repo.insert!()
  end

  @chars ~w(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)
  def random_asset_code() do
    int = :erlang.unique_integer([:positive, :monotonic])

    Enum.reduce(1..4, [int], fn _i, acc ->
      [Enum.random(@chars) | acc]
    end)
    |> Enum.join("")
  end
end
