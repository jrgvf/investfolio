defmodule Investfolio.Uol.ClientTest do
  use ExUnit.Case, async: false

  import Tesla.Mock, only: [mock_global: 1, json: 2]

  describe "Investfolio.Uol.Client" do
    alias Investfolio.Uol.{Asset, Client, QuoteInfo}

    @base_ul "https://cotacoes.economia.uol.com.br/ws"
    @assets_list_path "/asset/stock/list"
    @asset_intraday_path "/asset/1315/intraday"
    @asset_interday_path "/asset/1315/interday"

    @response_status 200
    @response_body %{
      data: [
        %{
          code: "A1AP34.SA",
          companyAbvName: "ADVANCE AUTO",
          companyName: "ADVANCE AUTO",
          idt: 2544,
          name: "ADVANCE AUTO"
        }
      ],
      timeOffSet: -10_800_000
    }
    test "assets_list with success" do
      mock_assets_list(@response_body, @response_status)
      assert {:ok, [asset_data]} = Client.assets_list()

      assert asset_data == %Asset{
               code: "A1AP34.SA",
               companyAbvName: "ADVANCE AUTO",
               companyName: "ADVANCE AUTO",
               idt: 2544,
               name: "ADVANCE AUTO"
             }
    end

    @response_status 200
    @response_body %{
      error: "FSW-0401",
      today: 1_585_920_080_733
    }
    test "assets_list with error in body" do
      mock_assets_list(@response_body, @response_status)

      assert {:warning, "Não há informação disponível para esta ação/índice."} =
               Client.assets_list()
    end

    @response_status 500
    @response_body %{}
    test "assets_list with status error" do
      mock_assets_list(@response_body, @response_status)
      assert {:error, %Tesla.Env{status: @response_status}} = Client.assets_list()
    end

    test "assets_list throw Exception" do
      mock_global(fn
        %{method: :get, url: @base_ul <> @assets_list_path} ->
          raise ArgumentError, "Testing!"
      end)

      assert_raise ArgumentError, "Testing!", fn -> Client.assets_list() end
    end

    @response_status 200
    @response_body %{
      data: [
        %{
          date: 1_585_944_540_000,
          high: 9.38,
          low: 9.11,
          price: 9.22,
          var: -0.51,
          varpct: -5.24,
          vol: 745_866.0
        },
        %{
          date: 1_585_943_640_000,
          high: 9.38,
          low: 9.11,
          price: 9.22,
          var: -0.51,
          varpct: -5.24,
          vol: 731_627.0
        }
      ],
      lastUpdate: 1_585_944_540_000,
      timeOffSet: -10_800_000,
      today: 1_585_951_944_644,
      type: "stock"
    }
    test "asset_intraday without size return success" do
      mock_asset_intraday(@response_body, @response_status)
      assert {:ok, [recent_intraday, previous_intraday]} = Client.asset_intraday("1315")

      assert recent_intraday == %QuoteInfo{
               date: ~U[2020-04-03 20:09:00Z],
               high: 9.38,
               low: 9.11,
               price: 9.22,
               var: -0.51,
               varpct: -5.24,
               vol: 745_866.0
             }

      assert previous_intraday == %QuoteInfo{
               date: ~U[2020-04-03 19:54:00Z],
               high: 9.38,
               low: 9.11,
               price: 9.22,
               var: -0.51,
               varpct: -5.24,
               vol: 731_627.0
             }
    end

    @response_status 200
    @response_body %{
      data: [
        %{
          date: 1_585_944_540_000,
          high: 9.38,
          low: 9.11,
          price: 9.22,
          var: -0.51,
          varpct: -5.24,
          vol: 745_866.0
        }
      ],
      lastUpdate: 1_585_944_540_000,
      timeOffSet: -10_800_000,
      today: 1_585_951_944_644,
      type: "stock"
    }
    test "asset_intraday with size 1 return success" do
      mock_asset_intraday(@response_body, @response_status, 1)
      assert {:ok, [intraday]} = Client.asset_intraday("1315", 1)

      assert intraday == %QuoteInfo{
               date: ~U[2020-04-03 20:09:00Z],
               high: 9.38,
               low: 9.11,
               price: 9.22,
               var: -0.51,
               varpct: -5.24,
               vol: 745_866.0
             }
    end

    @response_status 200
    @response_body %{
      error: "FSW-0401",
      today: 1_585_920_080_733
    }
    test "asset_intraday with error in body" do
      mock_asset_intraday(@response_body, @response_status)

      assert {:warning, "Não há informação disponível para esta ação/índice."} =
               Client.asset_intraday("1315")
    end

    @response_status 500
    @response_body %{}
    test "asset_intraday with status error" do
      mock_asset_intraday(@response_body, @response_status)
      assert {:error, %Tesla.Env{status: @response_status}} = Client.asset_intraday("1315")
    end

    test "asset_intraday throw Exception" do
      mock_global(fn
        %{method: :get, url: @base_ul <> @asset_intraday_path} ->
          raise ArgumentError, "Testing!"
      end)

      assert_raise ArgumentError, "Testing!", fn -> Client.asset_intraday("1315") end
    end

    @response_status 200
    @response_body %{
      data: [
        %{
          date: 1_585_882_800_000,
          high: 9.38,
          low: 9.11,
          price: 9.22,
          var: -0.51,
          varpct: -5.24,
          vol: 745_866.0
        },
        %{
          date: 1_585_796_400_000,
          high: 9.38,
          low: 9.11,
          price: 9.22,
          var: -0.51,
          varpct: -5.24,
          vol: 731_627.0
        }
      ],
      lastUpdate: 1_585_944_300_000,
      timeOffSet: -10_800_000,
      today: 1_586_011_561_000,
      type: "stock"
    }
    test "asset_interday without size return success" do
      mock_asset_interday(@response_body, @response_status)
      assert {:ok, [recent_intraday, previous_intraday]} = Client.asset_interday("1315")

      assert recent_intraday == %QuoteInfo{
               date: ~U[2020-04-03 03:00:00Z],
               high: 9.38,
               low: 9.11,
               price: 9.22,
               var: -0.51,
               varpct: -5.24,
               vol: 745_866.0
             }

      assert previous_intraday == %QuoteInfo{
               date: ~U[2020-04-02 03:00:00Z],
               high: 9.38,
               low: 9.11,
               price: 9.22,
               var: -0.51,
               varpct: -5.24,
               vol: 731_627.0
             }
    end

    @response_status 200
    @response_body %{
      data: [
        %{
          date: 1_585_882_800_000,
          high: 9.38,
          low: 9.11,
          price: 9.22,
          var: -0.51,
          varpct: -5.24,
          vol: 745_866.0
        }
      ],
      lastUpdate: 1_585_944_300_000,
      timeOffSet: -10_800_000,
      today: 1_586_011_561_000,
      type: "stock"
    }
    test "asset_interday with size 1 return success" do
      mock_asset_interday(@response_body, @response_status, 1)
      assert {:ok, [intraday]} = Client.asset_interday("1315", 1)

      assert intraday == %QuoteInfo{
               date: ~U[2020-04-03 03:00:00Z],
               high: 9.38,
               low: 9.11,
               price: 9.22,
               var: -0.51,
               varpct: -5.24,
               vol: 745_866.0
             }
    end

    @response_status 200
    @response_body %{
      error: "FSW-0401",
      today: 1_585_920_080_733
    }
    test "asset_interday with error in body" do
      mock_asset_interday(@response_body, @response_status)

      assert {:warning, "Não há informação disponível para esta ação/índice."} =
               Client.asset_interday("1315")
    end

    @response_status 500
    @response_body %{}
    test "asset_interday with status error" do
      mock_asset_interday(@response_body, @response_status)
      assert {:error, %Tesla.Env{status: @response_status}} = Client.asset_interday("1315")
    end

    test "asset_interday throw Exception" do
      mock_global(fn
        %{method: :get, url: @base_ul <> @asset_interday_path} ->
          raise ArgumentError, "Testing!"
      end)

      assert_raise ArgumentError, "Testing!", fn -> Client.asset_interday("1315") end
    end

    def mock_assets_list(response_body, response_status) do
      mock_global(fn
        %{method: :get, url: @base_ul <> @assets_list_path} ->
          json(response_body, status: response_status)
      end)
    end

    def mock_asset_intraday(response_body, response_status, size \\ nil)

    def mock_asset_intraday(response_body, response_status, nil) do
      mock_global(fn
        %{method: :get, url: @base_ul <> @asset_intraday_path} ->
          json(response_body, status: response_status)
      end)
    end

    def mock_asset_intraday(response_body, response_status, size) do
      mock_global(fn
        %{method: :get, url: @base_ul <> @asset_intraday_path, query: [size: ^size]} ->
          json(response_body, status: response_status)
      end)
    end

    def mock_asset_interday(response_body, response_status, size \\ nil)

    def mock_asset_interday(response_body, response_status, nil) do
      mock_global(fn
        %{method: :get, url: @base_ul <> @asset_interday_path} ->
          json(response_body, status: response_status)
      end)
    end

    def mock_asset_interday(response_body, response_status, size) do
      mock_global(fn
        %{method: :get, url: @base_ul <> @asset_interday_path, query: [size: ^size]} ->
          json(response_body, status: response_status)
      end)
    end
  end
end
