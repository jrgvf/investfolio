defmodule Investfolio.UsersTest do
  use Investfolio.DataCase

  alias Investfolio.Users
  alias Comeonin.Ecto.Password
  alias Investfolio.Repo

  describe "users" do
    alias Investfolio.Users.User

    @valid_attrs %{
      email: "foo@bar.com",
      email_confirmation: "foo@bar.com",
      name: "Foo User",
      password: "foo_bar!",
      password_confirmation: "foo_bar!"
    }
    @update_attrs %{
      email: "bar@foo.com",
      name: "Bar User",
      password: "bar_foo!"
    }
    @invalid_attrs %{email: nil, name: nil, password: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Users.create_user()

      user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Users.create_user(@valid_attrs)
      assert user.email == "foo@bar.com"
      assert user.name == "Foo User"
      assert Password.valid?("foo_bar!", user.password)
    end

    test "create_user/1 with invalid email returns error changeset" do
      assert {:error, changeset} =
               Users.create_user(
                 Map.merge(@valid_attrs, %{
                   email: "foo@bar.com.",
                   email_confirmation: "foo@bar.com."
                 })
               )

      assert "has invalid format" in errors_on(changeset).email
    end

    test "create_user/1 with without confirmation fields returns error changeset" do
      assert {:error, changeset} =
               Users.create_user(
                 Map.merge(@valid_attrs, %{
                   email_confirmation: "another",
                   password_confirmation: "another"
                 })
               )

      assert "does not match confirmation" in errors_on(changeset).password_confirmation
      assert "does not match confirmation" in errors_on(changeset).email_confirmation
    end

    test "create_user/1 with already used email returns error changeset" do
      Users.create_user(@valid_attrs)

      assert {:error, changeset} = Users.create_user(@valid_attrs)
      assert "has already been taken" in errors_on(changeset).email
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Users.create_user(@invalid_attrs)
    end

    test "authenticate/2 with valid data returns a user" do
      Users.create_user(@valid_attrs)
      assert {:ok, %User{email: "foo@bar.com"}} = Users.authenticate("foo@bar.com", "foo_bar!")
    end

    test "authenticate/2 with invalid email returns :error" do
      Users.create_user(@valid_attrs)
      assert :error = Users.authenticate("bar@foo.com", "bar_foo!")
    end

    test "authenticate/2 with invalid password returns :error" do
      Users.create_user(@valid_attrs)
      assert :error = Users.authenticate("foo@bar.com", "bar_foo!")
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Users.update_user(user, @update_attrs)
      assert user.email == "bar@foo.com"
      assert user.name == "Bar User"
      assert Password.valid?("bar_foo!", user.password)
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Users.update_user(user, @invalid_attrs)
      assert Repo.get!(User, user.id).email == user.email
    end

    test "get_user!/1 fetch the user with valid id returns a user" do
      user = user_fixture()
      assert %User{} = Users.get_user!(user.id)
    end

    test "get_user!/1 fetch the user with invalid id returns a user" do
      assert_raise Ecto.NoResultsError, fn -> Users.get_user!(Ecto.UUID.generate()) end
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Users.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Repo.get!(User, user.id) end
    end

    test "list_portfolios/1 fetch user portfolios" do
      user = user_fixture()
      user2 = Investfolio.Factory.create_user()
      portfolio = Investfolio.Factory.create_portfolio(user)

      assert [^portfolio] = Users.list_portfolios(user)
      assert [] = Users.list_portfolios(user2)
    end
  end
end
