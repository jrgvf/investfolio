defmodule Investfolio.PortfoliosTest do
  use Investfolio.DataCase

  alias Investfolio.Portfolios

  setup do
    user = Investfolio.Factory.create_user()
    mglu = Investfolio.Factory.create_asset("MGLU3")
    mxrf = Investfolio.Factory.create_asset("MXRF11")
    {:ok, %{user: user, mglu: mglu, mxrf: mxrf}}
  end

  describe "portfolios" do
    alias Investfolio.Portfolios.Portfolio

    @valid_attrs %{description: "some description", name: "some name", status: "active"}
    @update_attrs %{
      description: "some updated description",
      name: "some updated name",
      status: "archived"
    }
    @invalid_attrs %{description: nil, name: nil, status: nil}

    def portfolio_fixture(user, attrs \\ %{}) do
      attrs = Enum.into(attrs, @valid_attrs)
      {:ok, portfolio} = Portfolios.create_portfolio(user, attrs)

      portfolio
    end

    test "get_portfolio!/1 returns the portfolio with given id", %{user: user} do
      portfolio = portfolio_fixture(user)
      assert Portfolios.get_portfolio!(portfolio.id) == portfolio
    end

    test "create_portfolio/1 with valid data creates a portfolio", %{user: user} do
      assert {:ok, %Portfolio{} = portfolio} = Portfolios.create_portfolio(user, @valid_attrs)
      assert portfolio.description == "some description"
      assert portfolio.name == "some name"
      assert portfolio.status == :active
    end

    test "create_portfolio/1 with invalid data returns error changeset", %{user: user} do
      assert {:error, %Ecto.Changeset{}} = Portfolios.create_portfolio(user, @invalid_attrs)
    end

    test "update_portfolio/2 with valid data updates the portfolio", %{user: user} do
      portfolio = portfolio_fixture(user)

      assert {:ok, %Portfolio{} = portfolio} =
               Portfolios.update_portfolio(portfolio, @update_attrs)

      assert portfolio.description == "some updated description"
      assert portfolio.name == "some updated name"
      assert portfolio.status == :archived
    end

    test "update_portfolio/2 with invalid data returns error changeset", %{user: user} do
      portfolio = portfolio_fixture(user)

      assert {:error, %Ecto.Changeset{}} = Portfolios.update_portfolio(portfolio, @invalid_attrs)

      assert portfolio == Portfolios.get_portfolio!(portfolio.id)
    end

    test "delete_portfolio/1 deletes the portfolio", %{user: user} do
      portfolio = portfolio_fixture(user)
      assert {:ok, %Portfolio{}} = Portfolios.delete_portfolio(portfolio)
      assert_raise Ecto.NoResultsError, fn -> Portfolios.get_portfolio!(portfolio.id) end
    end

    test "change_portfolio/1 returns a portfolio changeset", %{user: user} do
      portfolio = portfolio_fixture(user)
      assert %Ecto.Changeset{} = Portfolios.change_portfolio(portfolio)
    end

    test "list_portfolio_users/1 fetch portfolio users", %{user: user} do
      portfolio = portfolio_fixture(user)
      user2 = Investfolio.Factory.create_user()
      Investfolio.Factory.create_portfolio_user(user2.id, portfolio.id, :reader)

      assert [portfolio_user, portfolio_user2] = Portfolios.list_portfolio_users(portfolio)

      assert portfolio_user.user_id == user.id
      assert portfolio_user.user_email == user.email
      assert portfolio_user.user_name == user.name
      assert portfolio_user.role == :owner

      assert portfolio_user2.user_id == user2.id
      assert portfolio_user2.user_email == user2.email
      assert portfolio_user2.user_name == user2.name
      assert portfolio_user2.role == :reader
    end
  end

  describe "movements" do
    alias Investfolio.Portfolios.Movement

    @valid_attrs %{
      asset_code: "MGLU3",
      average_price: "120.5",
      date: ~D[2010-04-17],
      description: "some description",
      operational_cost: "1.23",
      quantity: 10,
      type: "purchase"
    }
    @update_attrs %{
      description: "some updated description",
      isin: "some updated isin",
      type: "sale"
    }
    @invalid_attrs %{
      asset_code: nil,
      average_price: nil,
      date: nil,
      description: nil,
      isin: nil,
      operational_cost: nil,
      quantity: nil,
      total_value: nil,
      type: nil
    }

    def movement_fixture(portfolio, asset, attrs \\ %{}) do
      {:ok, movement} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Map.put(:portfolio_id, portfolio.id)
        |> Map.put(:asset_id, asset.id)
        |> Portfolios.create_movement()

      movement
    end

    test "list_movements/1 returns all portfolio movements", %{user: user, mglu: mglu} do
      portfolio = Investfolio.Factory.create_portfolio(user)
      movement = movement_fixture(portfolio, mglu)
      assert Portfolios.list_movements(portfolio) == [movement]
    end

    test "get_movement!/1 returns the movement with given id", %{user: user, mglu: mglu} do
      portfolio = Investfolio.Factory.create_portfolio(user)
      movement = movement_fixture(portfolio, mglu)
      assert Portfolios.get_movement!(movement.id) == movement
    end

    test "create_movement/1 with valid data creates a movement", %{user: user, mglu: mglu} do
      portfolio = Investfolio.Factory.create_portfolio(user)

      assert {:ok, %Movement{} = movement} =
               @valid_attrs
               |> Map.put(:portfolio_id, portfolio.id)
               |> Map.put(:asset_id, mglu.id)
               |> Portfolios.create_movement()

      assert movement.asset_code == "MGLU3"
      assert movement.average_price == Decimal.new("120.5")
      assert movement.date == ~D[2010-04-17]
      assert movement.description == "some description"
      assert movement.isin == nil
      assert movement.operational_cost == Decimal.new("1.23")
      assert movement.quantity == 10
      assert movement.total_value == Decimal.new("1206.23")
      assert movement.type == :purchase
    end

    test "create_movement/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Portfolios.create_movement(@invalid_attrs)
    end

    test "update_movement/2 with valid data updates the movement", %{user: user, mglu: mglu} do
      portfolio = Investfolio.Factory.create_portfolio(user)
      movement = movement_fixture(portfolio, mglu)
      assert {:ok, %Movement{} = movement} = Portfolios.update_movement(movement, @update_attrs)

      assert movement.asset_code == "MGLU3"
      assert movement.average_price == Decimal.new("120.5")
      assert movement.date == ~D[2010-04-17]
      assert movement.description == "some updated description"
      assert movement.isin == "some updated isin"
      assert movement.operational_cost == Decimal.new("1.23")
      assert movement.quantity == 10
      assert movement.total_value == Decimal.new("1203.77")
      assert movement.type == :sale
    end

    test "update_movement/2 with invalid data returns error changeset", %{user: user, mglu: mglu} do
      portfolio = Investfolio.Factory.create_portfolio(user)
      movement = movement_fixture(portfolio, mglu)
      assert {:error, %Ecto.Changeset{}} = Portfolios.update_movement(movement, @invalid_attrs)
      assert movement == Portfolios.get_movement!(movement.id)
    end

    test "delete_movement/1 deletes the movement", %{user: user, mglu: mglu} do
      portfolio = Investfolio.Factory.create_portfolio(user)
      movement = movement_fixture(portfolio, mglu)
      assert {:ok, %Movement{}} = Portfolios.delete_movement(movement)
      assert_raise Ecto.NoResultsError, fn -> Portfolios.get_movement!(movement.id) end
    end

    test "change_movement/1 returns a movement changeset", %{user: user, mglu: mglu} do
      portfolio = Investfolio.Factory.create_portfolio(user)
      movement = movement_fixture(portfolio, mglu)
      assert %Ecto.Changeset{} = Portfolios.change_movement(movement)
    end
  end

  describe "dividends" do
    alias Investfolio.Portfolios.Dividend

    @valid_attrs %{
      asset_code: "MXRF11",
      base_date: ~D[2020-03-31],
      payment_date: ~D[2020-04-15],
      quantity: 39,
      type: "income",
      value: "0.08"
    }
    @update_attrs %{
      isin: "some updated isin",
      quantity: 43,
      value: "0.09"
    }
    @invalid_attrs %{
      asset_code: nil,
      base_date: nil,
      isin: nil,
      payment_date: nil,
      quantity: nil,
      total_received: nil,
      type: nil,
      value: nil
    }

    def dividend_fixture(portfolio, asset, attrs \\ %{}) do
      {:ok, movement} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Map.put(:portfolio_id, portfolio.id)
        |> Map.put(:asset_id, asset.id)
        |> Portfolios.create_dividend()

      movement
    end

    test "list_dividends/1 returns all dividends", %{user: user, mxrf: mxrf} do
      portfolio = Investfolio.Factory.create_portfolio(user)
      dividend = dividend_fixture(portfolio, mxrf)
      assert Portfolios.list_dividends(portfolio) == [dividend]
    end

    test "get_dividend!/1 returns the dividend with given id", %{user: user, mxrf: mxrf} do
      portfolio = Investfolio.Factory.create_portfolio(user)
      dividend = dividend_fixture(portfolio, mxrf)
      assert Portfolios.get_dividend!(dividend.id) == dividend
    end

    test "create_dividend/1 with valid data creates a dividend", %{user: user, mxrf: mxrf} do
      portfolio = Investfolio.Factory.create_portfolio(user)

      assert {:ok, %Dividend{} = dividend} =
               @valid_attrs
               |> Map.put(:portfolio_id, portfolio.id)
               |> Map.put(:asset_id, mxrf.id)
               |> Portfolios.create_dividend()

      assert dividend.asset_code == "MXRF11"
      assert dividend.base_date == ~D[2020-03-31]
      assert dividend.isin == nil
      assert dividend.payment_date == ~D[2020-04-15]
      assert dividend.quantity == 39
      assert dividend.total_received == Decimal.new("3.12")
      assert dividend.type == :income
      assert dividend.value == Decimal.new("0.08")
      assert dividend.reference_month == 03
      assert dividend.reference_year == 2020
    end

    test "create_dividend/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Portfolios.create_dividend(@invalid_attrs)
    end

    test "update_dividend/2 with valid data updates the dividend", %{user: user, mxrf: mxrf} do
      portfolio = Investfolio.Factory.create_portfolio(user)
      dividend = dividend_fixture(portfolio, mxrf)

      assert {:ok, %Dividend{} = dividend} = Portfolios.update_dividend(dividend, @update_attrs)
      assert dividend.asset_code == "MXRF11"
      assert dividend.base_date == ~D[2020-03-31]
      assert dividend.isin == "some updated isin"
      assert dividend.payment_date == ~D[2020-04-15]
      assert dividend.quantity == 43
      assert dividend.total_received == Decimal.new("3.87")
      assert dividend.type == :income
      assert dividend.value == Decimal.new("0.09")
      assert dividend.reference_month == 03
      assert dividend.reference_year == 2020
    end

    test "update_dividend/2 with invalid data returns error changeset", %{user: user, mxrf: mxrf} do
      portfolio = Investfolio.Factory.create_portfolio(user)
      dividend = dividend_fixture(portfolio, mxrf)
      assert {:error, %Ecto.Changeset{}} = Portfolios.update_dividend(dividend, @invalid_attrs)
      assert dividend == Portfolios.get_dividend!(dividend.id)
    end

    test "delete_dividend/1 deletes the dividend", %{user: user, mxrf: mxrf} do
      portfolio = Investfolio.Factory.create_portfolio(user)
      dividend = dividend_fixture(portfolio, mxrf)
      assert {:ok, %Dividend{}} = Portfolios.delete_dividend(dividend)
      assert_raise Ecto.NoResultsError, fn -> Portfolios.get_dividend!(dividend.id) end
    end

    test "change_dividend/1 returns a dividend changeset", %{user: user, mxrf: mxrf} do
      portfolio = Investfolio.Factory.create_portfolio(user)
      dividend = dividend_fixture(portfolio, mxrf)
      assert %Ecto.Changeset{} = Portfolios.change_dividend(dividend)
    end
  end
end
