defmodule Investfolio.AssetsTest do
  use Investfolio.DataCase

  alias Investfolio.Assets

  describe "assets" do
    alias Investfolio.Assets.Asset

    @valid_attrs %{
      code: "MXRF11",
      company_abv_name: "FII MAXI REN CI",
      company_name: "FII MAXI REN CI",
      external_id: "1315",
      name: "FII MAXI REN",
      type: :fii
    }
    @update_attrs %{
      code: "MXRF11",
      company_abv_name: "UPDATED FII MAXI REN CI",
      company_name: "UPDATED FII MAXI REN CI",
      external_id: "1315",
      name: "UPDATED FII MAXI REN",
      type: :fii
    }
    @invalid_attrs %{
      code: nil,
      company_abv_name: nil,
      company_name: nil,
      external_id: nil,
      name: nil,
      type: nil
    }

    def asset_fixture(attrs \\ %{}) do
      {:ok, asset} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Assets.create_asset()

      asset
    end

    test "list_assets/0 returns all assets" do
      asset = asset_fixture()
      assert Assets.list_assets() == [asset]
    end

    test "get_asset!/1 returns the asset with given id" do
      asset = asset_fixture()
      assert Assets.get_asset!(asset.id) == asset
    end

    test "create_asset/1 with valid data creates a asset" do
      assert {:ok, %Asset{} = asset} = Assets.create_asset(@valid_attrs)
      assert asset.code == "MXRF11"
      assert asset.company_abv_name == "FII MAXI REN CI"
      assert asset.company_name == "FII MAXI REN CI"
      assert asset.external_id == "1315"
      assert asset.name == "FII MAXI REN"
      assert asset.type == :fii
    end

    test "create_asset/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Assets.create_asset(@invalid_attrs)
    end

    test "update_asset/2 with valid data updates the asset" do
      asset = asset_fixture()
      assert {:ok, %Asset{} = asset} = Assets.update_asset(asset, @update_attrs)
      assert asset.code == "MXRF11"
      assert asset.company_abv_name == "UPDATED FII MAXI REN CI"
      assert asset.company_name == "UPDATED FII MAXI REN CI"
      assert asset.external_id == "1315"
      assert asset.name == "UPDATED FII MAXI REN"
      assert asset.type == :fii
    end

    test "update_asset/2 with invalid data returns error changeset" do
      asset = asset_fixture()
      assert {:error, %Ecto.Changeset{}} = Assets.update_asset(asset, @invalid_attrs)
      assert asset == Assets.get_asset!(asset.id)
    end

    test "delete_asset/1 deletes the asset" do
      asset = asset_fixture()
      assert {:ok, %Asset{}} = Assets.delete_asset(asset)
      assert_raise Ecto.NoResultsError, fn -> Assets.get_asset!(asset.id) end
    end

    test "change_asset/1 returns a asset changeset" do
      asset = asset_fixture()
      assert %Ecto.Changeset{} = Assets.change_asset(asset)
    end
  end
end
