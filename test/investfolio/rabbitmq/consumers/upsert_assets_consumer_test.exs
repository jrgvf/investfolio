defmodule Investfolio.RabbitMQ.Consumers.UpsertAssetsConsumerTest do
  use Investfolio.DataCase, async: false

  describe "Investfolio.RabbitMQ.Consumers.UpsertAssetsConsumer" do
    alias Investfolio.RabbitMQ.Consumers.UpsertAssetsConsumer
    alias Investfolio.Assets.Asset

    @input_message "{\"code\":\"MGLU3\",\"company_abv_name\":\"MAG LUIZA ON\",\"company_name\":\"Magazine Luiza\",\"external_id\":\"1183\",\"name\":\"MAG LUIZA ON\",\"type\":\"stock\"}"
    test "Proccess and upsert Asset" do
      assert Investfolio.Repo.get_by(Asset, code: "MGLU3") == nil

      ref = Broadway.test_messages(UpsertAssetsConsumer, [@input_message])
      assert_receive {:ack, ^ref, [%{data: @input_message}], []}

      assert %Asset{} = asset = Investfolio.Repo.get_by(Asset, code: "MGLU3")
      assert asset.code == "MGLU3"
      assert asset.company_abv_name == "MAG LUIZA ON"
      assert asset.company_name == "Magazine Luiza"
      assert asset.external_id == "1183"
      assert asset.name == "MAG LUIZA ON"
      assert asset.type == :stock
    end

    @input_message "{\"code\":\"MGLU3\",\"company_abv_name\":\"MAG LUIZA ON\",\"company_name\":\"Magazine Luiza\",\"external_id\":1183,\"name\":\"MAG LUIZA ON\",\"type\":\"stock\"}"
    @tag :capture_log
    test "Proccess and fail with invalid asset data" do
      assert Investfolio.Repo.get_by(Asset, code: "MGLU3") == nil

      ref = Broadway.test_messages(UpsertAssetsConsumer, [@input_message])
      assert_receive {:ack, ^ref, [], [%{data: @input_message}]}

      assert Investfolio.Repo.get_by(Asset, code: "MGLU3") == nil
    end

    @input_message "invalid message"
    @tag :capture_log
    test "Proccess and fail with invalid message" do
      assert Investfolio.Repo.get_by(Asset, code: "MGLU3") == nil

      ref = Broadway.test_messages(UpsertAssetsConsumer, [@input_message])
      assert_receive {:ack, ^ref, [], [%{data: @input_message}]}

      assert Investfolio.Repo.get_by(Asset, code: "MGLU3") == nil
    end
  end
end
