defmodule Investfolio.RabbitMQ.Consumers.UolAssetsConsumerTest do
  use ExUnit.Case, async: false

  import Mock
  import Tesla.Mock, only: [mock_global: 1, json: 2]

  describe "Investfolio.RabbitMQ.Consumers.UolAssetsConsumer" do
    alias Investfolio.Uol.Asset
    alias Investfolio.RabbitMQ
    alias Investfolio.RabbitMQ.Consumers.UolAssetsConsumer

    @response_body_1183 %{
      data: [
        %{
          date: 1_585_944_660_000,
          high: 35.65,
          low: 33.33,
          price: 35.54,
          var: -0.35,
          varpct: -0.98,
          vol: 1.64001e7
        }
      ],
      lastUpdate: 1_585_944_660_000,
      timeOffSet: -10_800_000,
      today: 1_586_038_013_982,
      type: "stock"
    }
    @response_body_2559 %{error: "FSW-0401", today: 1_586_042_719_308}
    setup_with_mocks([
      {RabbitMQ.Manager, [],
       upsert_assets_queue_name: fn -> "upsert_queue" end, publish: fn _, _ -> :ok end}
    ]) do
      mock_global(fn
        %{method: :get, url: "https://cotacoes.economia.uol.com.br/ws/asset/1183/intraday"} ->
          json(@response_body_1183, status: 200)

        %{method: :get, url: "https://cotacoes.economia.uol.com.br/ws/asset/2559/intraday"} ->
          json(@response_body_2559, status: 200)

        %{method: :get, url: "https://cotacoes.economia.uol.com.br/ws/asset/1454/intraday"} ->
          raise ArgumentError, "Testing Error!"
      end)

      :ok
    end

    @input_message "{\"code\":\"MGLU3.SA\",\"companyAbvName\":\"MAG LUIZA ON    \",\"companyName\":\"Magazine Luiza\",\"idt\":1183,\"name\":\"MAG LUIZA ON\"}"
    @output_message "{\"code\":\"MGLU3\",\"company_abv_name\":\"MAG LUIZA ON\",\"company_name\":\"Magazine Luiza\",\"external_id\":\"1183\",\"name\":\"MAG LUIZA ON\",\"type\":\"stock\"}"
    test "Proccess and publish a message to upsert_queue" do
      ref = Broadway.test_messages(UolAssetsConsumer, [@input_message])
      assert_receive {:ack, ^ref, [%{data: {:ok, %Asset{code: "MGLU3.SA"}}}], []}

      assert called(RabbitMQ.Manager.upsert_assets_queue_name())
      assert called(RabbitMQ.Manager.publish("upsert_queue", @output_message))
    end

    @input_message "{\"idt\":2559,\"code\":\"A1ME34.SA\",\"name\":\"AMETEK INC\",\"companyName\":\"AMETEK INC\",\"companyAbvName\":\"AMETEK INC\"}"
    test "Proccess and filter invalid asset" do
      ref = Broadway.test_messages(UolAssetsConsumer, [@input_message])
      response = "Não há informação disponível para esta ação/índice."
      assert_receive {:ack, ^ref, [%{data: {:warning, ^response}}], []}

      refute called(RabbitMQ.Manager.upsert_assets_queue_name())
      refute called(RabbitMQ.Manager.publish(:_, :_))
    end

    @input_message "{\"idt\":1454,\"code\":\"AAPL34.SA\",\"name\":\"APPLE\",\"companyName\":\"APPLE\",\"companyAbvName\":\"APPLE\"}"
    @tag :capture_log
    test "Proccess and filter error" do
      ref = Broadway.test_messages(UolAssetsConsumer, [@input_message])

      assert_receive {:ack, ^ref, [],
                      [
                        %{
                          data: @input_message,
                          status: {:error, %ArgumentError{message: "Testing Error!"}, _}
                        }
                      ]}

      refute called(RabbitMQ.Manager.upsert_assets_queue_name())
      refute called(RabbitMQ.Manager.publish(:_, :_))
    end
  end
end
