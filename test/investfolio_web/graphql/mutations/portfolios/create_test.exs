defmodule InvestfolioWeb.GraphQL.Mutations.Portfolios.CreateTest do
  use InvestfolioWeb.ConnCase, async: true

  setup do
    user = Investfolio.Factory.create_user()
    {:ok, %{user: user}}
  end

  @create_portfolio_mutation """
  mutation ($input: CreatePortfolioInput!){
    createPortfolio(input: $input){
      success
      portfolio {name description status}
      errors {key message}
    }
  }
  """
  test "createPortfolio field creates a portfolio", %{conn: conn, user: user} do
    variables = %{
      "input" => %{
        "name" => "Test Portfolio",
        "description" => "Test Portfolio"
      }
    }

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @create_portfolio_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "data" => %{
               "createPortfolio" => %{
                 "errors" => nil,
                 "success" => true,
                 "portfolio" => %{
                   "name" => "Test Portfolio",
                   "description" => "Test Portfolio",
                   "status" => "ACTIVE"
                 }
               }
             }
           }
  end

  test "creating a portfolio with empty name fails", %{conn: conn, user: user} do
    variables = %{
      "input" => %{
        "name" => "",
        "description" => "Test Portfolio"
      }
    }

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @create_portfolio_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "data" => %{
               "createPortfolio" => %{
                 "errors" => [%{"key" => "name", "message" => "can't be blank"}],
                 "success" => false,
                 "portfolio" => nil
               }
             }
           }
  end

  test "creating a portfolio with invalid status fails", %{conn: conn, user: user} do
    variables = %{
      "input" => %{
        "name" => "Test Portfolio",
        "status" => "INVALID"
      }
    }

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @create_portfolio_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "errors" => [
               %{
                 "message" =>
                   "Argument \"input\" has invalid value $input.\nIn field \"status\": Expected type \"PortfoliosStatus\", found \"INVALID\".",
                 "locations" => []
               }
             ]
           }
  end

  test "creating a portfolio without logged user fails", %{conn: conn} do
    variables = %{
      "input" => %{
        "name" => "Test Portfolio"
      }
    }

    conn = post(conn, "/api/", query: @create_portfolio_mutation, variables: variables)

    assert %{
             "data" => %{"createPortfolio" => nil},
             "errors" => [
               %{
                 "message" => "unauthorized"
               }
             ]
           } = json_response(conn, 200)
  end
end
