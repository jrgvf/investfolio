defmodule InvestfolioWeb.GraphQL.Mutations.Portfolios.RemoveTest do
  use InvestfolioWeb.ConnCase, async: true

  alias Investfolio.Portfolios

  setup do
    user = Investfolio.Factory.create_user()
    portfolio = Investfolio.Factory.create_portfolio(user)
    {:ok, %{user: user, portfolio: portfolio}}
  end

  @remove_portfolio_mutation """
  mutation ($input: RemovePortfolioInput!){
    removePortfolio(input: $input){
      success
      portfolio {name description status}
    }
  }
  """
  test "removePortfolio field removes a portfolio", %{
    conn: conn,
    user: user,
    portfolio: portfolio
  } do
    variables = %{"input" => %{"portfolioId" => portfolio.id}}

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @remove_portfolio_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "data" => %{
               "removePortfolio" => %{
                 "success" => true,
                 "portfolio" => %{
                   "name" => portfolio.name,
                   "description" => portfolio.description,
                   "status" => "ACTIVE"
                 }
               }
             }
           }

    assert_raise Ecto.NoResultsError, fn -> Portfolios.get_portfolio!(portfolio.id) end
  end

  test "removing a portfolio nonexistent", %{conn: conn, user: user} do
    variables = %{"input" => %{"portfolioId" => Ecto.UUID.generate()}}

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @remove_portfolio_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "data" => %{"removePortfolio" => nil},
             "errors" => [
               %{
                 "message" => "Portfolio not found",
                 "path" => ["removePortfolio"],
                 "locations" => [%{"column" => 0, "line" => 2}]
               }
             ]
           }
  end

  test "removing a portfolio without logged user fails", %{conn: conn, portfolio: portfolio} do
    variables = %{"input" => %{"portfolioId" => portfolio.id}}

    conn = post(conn, "/api/", query: @remove_portfolio_mutation, variables: variables)

    assert %{
             "data" => %{"removePortfolio" => nil},
             "errors" => [
               %{
                 "message" => "unauthorized"
               }
             ]
           } = json_response(conn, 200)
  end
end
