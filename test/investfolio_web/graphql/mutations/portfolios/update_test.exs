defmodule InvestfolioWeb.GraphQL.Mutations.Portfolios.UpdateTest do
  use InvestfolioWeb.ConnCase, async: true

  setup do
    user = Investfolio.Factory.create_user()
    portfolio = Investfolio.Factory.create_portfolio(user)
    {:ok, %{user: user, portfolio: portfolio}}
  end

  @update_portfolio_mutation """
  mutation ($input: UpdatePortfolioInput!){
    updatePortfolio(input: $input){
      success
      portfolio {name description status}
      errors {key message}
    }
  }
  """
  test "updatePortfolio field updates a portfolio", %{
    conn: conn,
    user: user,
    portfolio: portfolio
  } do
    variables = %{
      "input" => %{
        "portfolioId" => portfolio.id,
        "name" => "Test Portfolio",
        "description" => "Test Portfolio"
      }
    }

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @update_portfolio_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "data" => %{
               "updatePortfolio" => %{
                 "errors" => nil,
                 "success" => true,
                 "portfolio" => %{
                   "name" => "Test Portfolio",
                   "description" => "Test Portfolio",
                   "status" => "ACTIVE"
                 }
               }
             }
           }
  end

  test "updating a portfolio with empty name fails", %{
    conn: conn,
    user: user,
    portfolio: portfolio
  } do
    variables = %{
      "input" => %{
        "portfolioId" => portfolio.id,
        "name" => "",
        "description" => "Test Portfolio"
      }
    }

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @update_portfolio_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "data" => %{
               "updatePortfolio" => %{
                 "errors" => [%{"key" => "name", "message" => "can't be blank"}],
                 "success" => false,
                 "portfolio" => nil
               }
             }
           }
  end

  test "updating a portfolio with invalid status fails", %{
    conn: conn,
    user: user,
    portfolio: portfolio
  } do
    variables = %{
      "input" => %{
        "portfolioId" => portfolio.id,
        "name" => "Test Portfolio",
        "status" => "INVALID"
      }
    }

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @update_portfolio_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "errors" => [
               %{
                 "message" =>
                   "Argument \"input\" has invalid value $input.\nIn field \"status\": Expected type \"PortfoliosStatus\", found \"INVALID\".",
                 "locations" => []
               }
             ]
           }
  end

  test "updating a portfolio without logged user fails", %{conn: conn, portfolio: portfolio} do
    variables = %{
      "input" => %{
        "portfolioId" => portfolio.id,
        "name" => "Test Portfolio"
      }
    }

    conn = post(conn, "/api/", query: @update_portfolio_mutation, variables: variables)

    assert %{
             "data" => %{"updatePortfolio" => nil},
             "errors" => [
               %{
                 "message" => "unauthorized"
               }
             ]
           } = json_response(conn, 200)
  end
end
