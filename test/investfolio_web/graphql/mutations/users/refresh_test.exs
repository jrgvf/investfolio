defmodule InvestfolioWeb.GraphQL.Mutations.Users.RefreshTest do
  use InvestfolioWeb.ConnCase, async: true

  @refresh_mutation """
  mutation ($refreshToken: String!, $accessToken: String!){
    refresh(refreshToken: $refreshToken, accessToken: $accessToken) {
      accessToken
      refreshToken
    }
  }
  """
  test "return a user session", %{conn: conn} do
    user = Investfolio.Factory.create_user("super_secret")

    {:ok, access_token, _} = Investfolio.Auth.encode_and_sign(user, %{}, token_type: "access")
    {:ok, refresh_token, _} = Investfolio.Auth.encode_and_sign(user, %{}, token_type: "refresh")

    variables = %{"refreshToken" => refresh_token, "accessToken" => access_token}
    conn = post(conn, "/api/", query: @refresh_mutation, variables: variables)

    assert %{
             "data" => %{
               "refresh" => %{
                 "accessToken" => new_access_token,
                 "refreshToken" => new_refresh_token
               }
             }
           } = json_response(conn, 200)

    assert {:error, :token_not_found} = Investfolio.Auth.resource_from_token(access_token)
    assert {:error, :token_not_found} = Investfolio.Auth.resource_from_token(refresh_token)

    assert {:ok, ^user, _claims} = Investfolio.Auth.resource_from_token(new_access_token)
    assert {:ok, ^user, _claims} = Investfolio.Auth.resource_from_token(new_refresh_token)
  end

  test "refreshing with invalid data", %{conn: conn} do
    user = Investfolio.Factory.create_user("super_secret")

    {:ok, access_token, _} = Investfolio.Auth.encode_and_sign(user, %{}, token_type: "access")

    variables = %{"refreshToken" => access_token, "accessToken" => access_token}
    conn = post(conn, "/api/", query: @refresh_mutation, variables: variables)

    assert %{
             "data" => %{
               "refresh" => nil
             },
             "errors" => [
               %{
                 "message" => "invalid or expired refresh token"
               }
             ]
           } = json_response(conn, 200)
  end
end
