defmodule InvestfolioWeb.GraphQL.Mutations.Users.RevokeTest do
  use InvestfolioWeb.ConnCase, async: true

  @revoke_mutation """
  mutation ($refreshToken: String!, $accessToken: String!){
    revoke(refreshToken: $refreshToken, accessToken: $accessToken) {
      result
    }
  }
  """
  test "return a result", %{conn: conn} do
    user = Investfolio.Factory.create_user("super_secret")

    {:ok, access_token, _} = Investfolio.Auth.encode_and_sign(user, %{}, token_type: "access")
    {:ok, refresh_token, _} = Investfolio.Auth.encode_and_sign(user, %{}, token_type: "refresh")

    assert {:ok, ^user, _} = Investfolio.Auth.resource_from_token(access_token)
    assert {:ok, ^user, _} = Investfolio.Auth.resource_from_token(refresh_token)

    variables = %{"refreshToken" => refresh_token, "accessToken" => access_token}
    conn = post(conn, "/api/", query: @revoke_mutation, variables: variables)

    assert %{
             "data" => %{
               "revoke" => %{
                 "result" => "tokens revoked"
               }
             }
           } = json_response(conn, 200)

    assert {:error, :token_not_found} = Investfolio.Auth.resource_from_token(access_token)
    assert {:error, :token_not_found} = Investfolio.Auth.resource_from_token(refresh_token)
  end
end
