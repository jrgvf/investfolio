defmodule InvestfolioWeb.GraphQL.Mutations.Users.CreateUserTest do
  use InvestfolioWeb.ConnCase, async: true

  @create_user_mutation """
  mutation ($input: CreateUserInput!){
    createUser(input: $input){
      errors {key message}
      success
      user {name email}
    }
  }
  """
  test "createUser field creates an user", %{conn: conn} do
    variables = %{
      "input" => %{
        "name" => "Jorge Rodrigues",
        "email" => "jorge.rodrigues@example.com",
        "emailConfirmation" => "jorge.rodrigues@example.com",
        "password" => "super_secret",
        "passwordConfirmation" => "super_secret"
      }
    }

    conn = post(conn, "/api/", query: @create_user_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "data" => %{
               "createUser" => %{
                 "errors" => nil,
                 "success" => true,
                 "user" => %{
                   "email" => "jorge.rodrigues@example.com",
                   "name" => "Jorge Rodrigues"
                 }
               }
             }
           }
  end

  test "creating an user with an existing email fails", %{conn: conn} do
    user = Investfolio.Factory.create_user()

    variables = %{
      "input" => %{
        "name" => user.name,
        "email" => user.email,
        "emailConfirmation" => user.email,
        "password" => "super_secret",
        "passwordConfirmation" => "super_secret"
      }
    }

    conn = post(conn, "/api/", query: @create_user_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "data" => %{
               "createUser" => %{
                 "errors" => [%{"key" => "email", "message" => "has already been taken"}],
                 "success" => false,
                 "user" => nil
               }
             }
           }
  end

  test "creating an user with invalid data fails", %{conn: conn} do
    variables = %{
      "input" => %{
        "name" => "Jorge Rodrigues",
        "email" => "jorge.rodrigues@example.com.",
        "emailConfirmation" => "wrong_email",
        "password" => "short",
        "passwordConfirmation" => "wrong_password"
      }
    }

    conn = post(conn, "/api/", query: @create_user_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "data" => %{
               "createUser" => %{
                 "errors" => [
                   %{"key" => "email", "message" => "has invalid format"},
                   %{"key" => "email_confirmation", "message" => "does not match confirmation"},
                   %{"key" => "password", "message" => "should be at least 8 character(s)"},
                   %{"key" => "password_confirmation", "message" => "does not match confirmation"}
                 ],
                 "success" => false,
                 "user" => nil
               }
             }
           }
  end
end
