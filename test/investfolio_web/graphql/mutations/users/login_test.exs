defmodule InvestfolioWeb.GraphQL.Mutations.Users.LoginTest do
  use InvestfolioWeb.ConnCase, async: true

  @login_mutation """
  mutation ($email: String!, $password: String!){
    login(email: $email, password:$password) {
      accessToken
      refreshToken
    }
  }
  """
  test "creating a user session", %{conn: conn} do
    user = Investfolio.Factory.create_user("super_secret")

    variables = %{
      "email" => user.email,
      "password" => "super_secret"
    }

    conn = post(conn, "/api/", query: @login_mutation, variables: variables)

    assert %{
             "data" => %{
               "login" => %{
                 "accessToken" => access_token,
                 "refreshToken" => refresh_token
               }
             }
           } = json_response(conn, 200)

    assert {:ok, ^user, _claims} = Investfolio.Auth.resource_from_token(access_token)
    assert {:ok, ^user, _claims} = Investfolio.Auth.resource_from_token(refresh_token)
  end

  test "creating a user session with invalid data", %{conn: conn} do
    user = Investfolio.Factory.create_user("super_secret")

    variables = %{
      "email" => user.email,
      "password" => "wrong"
    }

    conn = post(conn, "/api/", query: @login_mutation, variables: variables)

    assert %{
             "data" => %{
               "login" => nil
             },
             "errors" => [
               %{
                 "message" => "incorrect email or password"
               }
             ]
           } = json_response(conn, 200)
  end

  test "creating a user session with nonexistent user", %{conn: conn} do
    variables = %{
      "email" => "jorge.rodrigues@example.com",
      "password" => "super_secret"
    }

    conn = post(conn, "/api/", query: @login_mutation, variables: variables)

    assert %{
             "data" => %{
               "login" => nil
             },
             "errors" => [
               %{
                 "message" => "incorrect email or password"
               }
             ]
           } = json_response(conn, 200)
  end
end
