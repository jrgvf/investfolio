defmodule InvestfolioWeb.GraphQL.Mutations.Dividends.UpdateTest do
  use InvestfolioWeb.ConnCase, async: true

  setup do
    user = Investfolio.Factory.create_user()
    portfolio = Investfolio.Factory.create_portfolio(user)
    mxrf = Investfolio.Factory.create_asset("MXRF11")
    dividend = Investfolio.Factory.create_dividend(mxrf, portfolio.id)
    {:ok, %{user: user, dividend: dividend}}
  end

  @update_dividend_mutation """
  mutation ($input: UpdateDividendInput!){
    updateDividend(input: $input){
      success
      dividend {assetCode type referenceMonth referenceYear totalReceived}
      errors {key message}
    }
  }
  """
  test "updateDividend field updates a dividend", %{conn: conn, user: user, dividend: dividend} do
    variables = %{
      "input" => %{
        "assetCode" => "MXRF11",
        "value" => "0.09",
        "baseDate" => "2020-03-31",
        "paymentDate" => "2020-04-15",
        "portfolioId" => dividend.portfolio_id,
        "dividendId" => dividend.id,
        "quantity" => 39,
        "type" => "BONUS"
      }
    }

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @update_dividend_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "data" => %{
               "updateDividend" => %{
                 "errors" => nil,
                 "success" => true,
                 "dividend" => %{
                   "assetCode" => "MXRF11",
                   "type" => "BONUS",
                   "referenceMonth" => 03,
                   "referenceYear" => 2020,
                   "totalReceived" => "3.51"
                 }
               }
             }
           }
  end

  test "updating a dividend with negative quantity fails", %{
    conn: conn,
    user: user,
    dividend: dividend
  } do
    variables = %{
      "input" => %{
        "assetCode" => "MXRF11",
        "value" => "0.09",
        "baseDate" => "2020-03-31",
        "paymentDate" => "2020-04-15",
        "portfolioId" => dividend.portfolio_id,
        "dividendId" => dividend.id,
        "quantity" => -39,
        "type" => "BONUS"
      }
    }

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @update_dividend_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "data" => %{
               "updateDividend" => %{
                 "errors" => [%{"key" => "quantity", "message" => "must be greater than 0"}],
                 "success" => false,
                 "dividend" => nil
               }
             }
           }
  end

  test "updating a dividend with invalid type fails", %{
    conn: conn,
    user: user,
    dividend: dividend
  } do
    variables = %{
      "input" => %{
        "assetCode" => "MXRF11",
        "value" => "0.09",
        "baseDate" => "2020-03-31",
        "paymentDate" => "2020-04-15",
        "portfolioId" => dividend.portfolio_id,
        "dividendId" => dividend.id,
        "quantity" => 39,
        "type" => "INVALID"
      }
    }

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @update_dividend_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "errors" => [
               %{
                 "message" =>
                   "Argument \"input\" has invalid value $input.\nIn field \"type\": Expected type \"DividendsType!\", found \"INVALID\".",
                 "locations" => []
               }
             ]
           }
  end

  test "updating a dividend without logged user fails", %{conn: conn, dividend: dividend} do
    variables = %{
      "input" => %{
        "assetCode" => "MXRF11",
        "value" => "0.09",
        "baseDate" => "2020-03-31",
        "paymentDate" => "2020-04-15",
        "portfolioId" => dividend.portfolio_id,
        "dividendId" => dividend.id,
        "quantity" => 39,
        "type" => "BONUS"
      }
    }

    conn = post(conn, "/api/", query: @update_dividend_mutation, variables: variables)

    assert %{
             "data" => %{"updateDividend" => nil},
             "errors" => [
               %{
                 "message" => "unauthorized"
               }
             ]
           } = json_response(conn, 200)
  end
end
