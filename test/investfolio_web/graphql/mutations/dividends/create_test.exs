defmodule InvestfolioWeb.GraphQL.Mutations.Dividends.CreateTest do
  use InvestfolioWeb.ConnCase, async: true

  setup do
    user = Investfolio.Factory.create_user()
    portfolio = Investfolio.Factory.create_portfolio(user)
    Investfolio.Factory.create_asset("MXRF11")
    {:ok, %{user: user, portfolio: portfolio}}
  end

  @create_dividend_mutation """
  mutation ($input: CreateDividendInput!){
    createDividend(input: $input){
      success
      dividend {assetCode type referenceMonth referenceYear totalReceived}
      errors {key message}
    }
  }
  """
  test "createDividend field creates a dividend", %{conn: conn, user: user, portfolio: portfolio} do
    variables = %{
      "input" => %{
        "assetCode" => "MXRF11",
        "value" => "0.08",
        "baseDate" => "2020-03-31",
        "paymentDate" => "2020-04-15",
        "portfolioId" => portfolio.id,
        "quantity" => 10
      }
    }

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @create_dividend_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "data" => %{
               "createDividend" => %{
                 "errors" => nil,
                 "success" => true,
                 "dividend" => %{
                   "assetCode" => "MXRF11",
                   "type" => "INCOME",
                   "referenceMonth" => 03,
                   "referenceYear" => 2020,
                   "totalReceived" => "0.80"
                 }
               }
             }
           }
  end

  test "creating a dividend with negative quantity fails", %{
    conn: conn,
    user: user,
    portfolio: portfolio
  } do
    variables = %{
      "input" => %{
        "assetCode" => "MXRF11",
        "value" => "0.08",
        "baseDate" => "2020-03-31",
        "paymentDate" => "2020-04-15",
        "portfolioId" => portfolio.id,
        "quantity" => -10
      }
    }

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @create_dividend_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "data" => %{
               "createDividend" => %{
                 "errors" => [%{"key" => "quantity", "message" => "must be greater than 0"}],
                 "success" => false,
                 "dividend" => nil
               }
             }
           }
  end

  test "creating a dividend with invalid type fails", %{
    conn: conn,
    user: user,
    portfolio: portfolio
  } do
    variables = %{
      "input" => %{
        "assetCode" => "MXRF11",
        "value" => "0.08",
        "baseDate" => "2020-03-31",
        "paymentDate" => "2020-04-15",
        "portfolioId" => portfolio.id,
        "quantity" => 10,
        "type" => "INVALID"
      }
    }

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @create_dividend_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "errors" => [
               %{
                 "message" =>
                   "Argument \"input\" has invalid value $input.\nIn field \"type\": Expected type \"DividendsType!\", found \"INVALID\".",
                 "locations" => []
               }
             ]
           }
  end

  test "creating a dividend without logged user fails", %{conn: conn, portfolio: portfolio} do
    variables = %{
      "input" => %{
        "assetCode" => "MXRF11",
        "value" => "0.08",
        "baseDate" => "2020-03-31",
        "paymentDate" => "2020-04-15",
        "portfolioId" => portfolio.id,
        "quantity" => 10
      }
    }

    conn = post(conn, "/api/", query: @create_dividend_mutation, variables: variables)

    assert %{
             "data" => %{"createDividend" => nil},
             "errors" => [
               %{
                 "message" => "unauthorized"
               }
             ]
           } = json_response(conn, 200)
  end
end
