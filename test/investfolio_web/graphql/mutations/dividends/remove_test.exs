defmodule InvestfolioWeb.GraphQL.Mutations.Dividends.RemoveTest do
  use InvestfolioWeb.ConnCase, async: true

  alias Investfolio.Portfolios

  setup do
    user = Investfolio.Factory.create_user()
    portfolio = Investfolio.Factory.create_portfolio(user)
    mxrf = Investfolio.Factory.create_asset("MXRF11")
    dividend = Investfolio.Factory.create_dividend(mxrf, portfolio.id)
    {:ok, %{user: user, dividend: dividend}}
  end

  @remove_dividend_mutation """
  mutation ($input: RemoveDividendInput!){
    removeDividend(input: $input){
      success
      dividend {id assetCode}
    }
  }
  """
  test "removeDividend field removes a dividend", %{conn: conn, user: user, dividend: dividend} do
    variables = %{
      "input" => %{"portfolioId" => dividend.portfolio_id, "dividendId" => dividend.id}
    }

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @remove_dividend_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "data" => %{
               "removeDividend" => %{
                 "success" => true,
                 "dividend" => %{
                   "id" => dividend.id,
                   "assetCode" => dividend.asset_code
                 }
               }
             }
           }

    assert_raise Ecto.NoResultsError, fn -> Portfolios.get_dividend!(dividend.id) end
  end

  test "removing a dividend nonexistent", %{conn: conn, user: user, dividend: dividend} do
    variables = %{
      "input" => %{"portfolioId" => dividend.portfolio_id, "dividendId" => Ecto.UUID.generate()}
    }

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @remove_dividend_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "data" => %{"removeDividend" => nil},
             "errors" => [
               %{
                 "message" => "Dividend not found",
                 "path" => ["removeDividend"],
                 "locations" => [%{"column" => 0, "line" => 2}]
               }
             ]
           }
  end

  test "removing a portfolio without logged user fails", %{conn: conn, dividend: dividend} do
    variables = %{
      "input" => %{"portfolioId" => dividend.portfolio_id, "dividendId" => dividend.id}
    }

    conn = post(conn, "/api/", query: @remove_dividend_mutation, variables: variables)

    assert %{
             "data" => %{"removeDividend" => nil},
             "errors" => [
               %{
                 "message" => "unauthorized"
               }
             ]
           } = json_response(conn, 200)
  end
end
