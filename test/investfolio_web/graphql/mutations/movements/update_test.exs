defmodule InvestfolioWeb.GraphQL.Mutations.Movements.UpdateTest do
  use InvestfolioWeb.ConnCase, async: true

  setup do
    user = Investfolio.Factory.create_user()
    portfolio = Investfolio.Factory.create_portfolio(user)
    mglu = Investfolio.Factory.create_asset("MGLU3")
    movement = Investfolio.Factory.create_movement(mglu, portfolio.id)
    {:ok, %{user: user, movement: movement}}
  end

  @update_movement_mutation """
  mutation ($input: UpdateMovementInput!){
    updateMovement(input: $input){
      success
      movement {assetCode date quantity totalValue type}
      errors {key message}
    }
  }
  """
  test "updateMovement field updates a movement", %{conn: conn, user: user, movement: movement} do
    variables = %{
      "input" => %{
        "assetCode" => "MGLU3",
        "averagePrice" => "9.99",
        "date" => "2020-04-09",
        "movementId" => movement.id,
        "operationalCost" => "1.23",
        "portfolioId" => movement.portfolio_id,
        "quantity" => 10,
        "type" => "SALE"
      }
    }

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @update_movement_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "data" => %{
               "updateMovement" => %{
                 "errors" => nil,
                 "success" => true,
                 "movement" => %{
                   "assetCode" => "MGLU3",
                   "date" => "2020-04-09",
                   "quantity" => 10,
                   "totalValue" => "98.67",
                   "type" => "SALE"
                 }
               }
             }
           }
  end

  test "updating a movement with negative quantity fails", %{
    conn: conn,
    user: user,
    movement: movement
  } do
    variables = %{
      "input" => %{
        "assetCode" => "MGLU3",
        "averagePrice" => "9.99",
        "date" => "2020-04-09",
        "movementId" => movement.id,
        "operationalCost" => "1.23",
        "portfolioId" => movement.portfolio_id,
        "quantity" => -10,
        "type" => "SALE"
      }
    }

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @update_movement_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "data" => %{
               "updateMovement" => %{
                 "errors" => [%{"key" => "quantity", "message" => "must be greater than 0"}],
                 "success" => false,
                 "movement" => nil
               }
             }
           }
  end

  test "updating a movement with invalid type fails", %{
    conn: conn,
    user: user,
    movement: movement
  } do
    variables = %{
      "input" => %{
        "assetCode" => "MGLU3",
        "averagePrice" => "9.99",
        "date" => "2020-04-09",
        "movementId" => movement.id,
        "operationalCost" => "1.23",
        "portfolioId" => movement.portfolio_id,
        "quantity" => 10,
        "type" => "INVALID"
      }
    }

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @update_movement_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "errors" => [
               %{
                 "message" =>
                   "Argument \"input\" has invalid value $input.\nIn field \"type\": Expected type \"MovementsType!\", found \"INVALID\".",
                 "locations" => []
               }
             ]
           }
  end

  test "updating a movement without logged user fails", %{conn: conn, movement: movement} do
    variables = %{
      "input" => %{
        "assetCode" => "MGLU3",
        "averagePrice" => "9.99",
        "date" => "2020-04-09",
        "movementId" => movement.id,
        "operationalCost" => "1.23",
        "portfolioId" => movement.portfolio_id,
        "quantity" => 10,
        "type" => "SALE"
      }
    }

    conn = post(conn, "/api/", query: @update_movement_mutation, variables: variables)

    assert %{
             "data" => %{"updateMovement" => nil},
             "errors" => [
               %{
                 "message" => "unauthorized"
               }
             ]
           } = json_response(conn, 200)
  end
end
