defmodule InvestfolioWeb.GraphQL.Mutations.Movements.RemoveTest do
  use InvestfolioWeb.ConnCase, async: true

  alias Investfolio.Portfolios

  setup do
    user = Investfolio.Factory.create_user()
    portfolio = Investfolio.Factory.create_portfolio(user)
    mglu = Investfolio.Factory.create_asset("MGLU3")
    movement = Investfolio.Factory.create_movement(mglu, portfolio.id)
    {:ok, %{user: user, movement: movement}}
  end

  @remove_movement_mutation """
  mutation ($input: RemoveMovementInput!){
    removeMovement(input: $input){
      success
      movement {id assetCode}
    }
  }
  """
  test "removeMovement field removes a movement", %{conn: conn, user: user, movement: movement} do
    variables = %{
      "input" => %{"portfolioId" => movement.portfolio_id, "movementId" => movement.id}
    }

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @remove_movement_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "data" => %{
               "removeMovement" => %{
                 "success" => true,
                 "movement" => %{
                   "id" => movement.id,
                   "assetCode" => movement.asset_code
                 }
               }
             }
           }

    assert_raise Ecto.NoResultsError, fn -> Portfolios.get_movement!(movement.id) end
  end

  test "removing a movement nonexistent", %{conn: conn, user: user, movement: movement} do
    variables = %{
      "input" => %{"portfolioId" => movement.portfolio_id, "movementId" => Ecto.UUID.generate()}
    }

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @remove_movement_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "data" => %{"removeMovement" => nil},
             "errors" => [
               %{
                 "message" => "Movement not found",
                 "path" => ["removeMovement"],
                 "locations" => [%{"column" => 0, "line" => 2}]
               }
             ]
           }
  end

  test "removing a portfolio without logged user fails", %{conn: conn, movement: movement} do
    variables = %{
      "input" => %{"portfolioId" => movement.portfolio_id, "movementId" => movement.id}
    }

    conn = post(conn, "/api/", query: @remove_movement_mutation, variables: variables)

    assert %{
             "data" => %{"removeMovement" => nil},
             "errors" => [
               %{
                 "message" => "unauthorized"
               }
             ]
           } = json_response(conn, 200)
  end
end
