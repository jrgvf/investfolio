defmodule InvestfolioWeb.GraphQL.Mutations.Movements.CreateTest do
  use InvestfolioWeb.ConnCase, async: true

  setup do
    user = Investfolio.Factory.create_user()
    portfolio = Investfolio.Factory.create_portfolio(user)
    Investfolio.Factory.create_asset("MGLU3")
    {:ok, %{user: user, portfolio: portfolio}}
  end

  @create_movement_mutation """
  mutation ($input: CreateMovementInput!){
    createMovement(input: $input){
      success
      movement {assetCode date quantity totalValue}
      errors {key message}
    }
  }
  """
  test "createMovement field creates a movement", %{conn: conn, user: user, portfolio: portfolio} do
    variables = %{
      "input" => %{
        "assetCode" => "MGLU3",
        "averagePrice" => "9.99",
        "date" => "2020-04-09",
        "operationalCost" => "1.23",
        "portfolioId" => portfolio.id,
        "quantity" => 10,
        "type" => "PURCHASE"
      }
    }

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @create_movement_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "data" => %{
               "createMovement" => %{
                 "errors" => nil,
                 "success" => true,
                 "movement" => %{
                   "assetCode" => "MGLU3",
                   "date" => "2020-04-09",
                   "quantity" => 10,
                   "totalValue" => "101.13"
                 }
               }
             }
           }
  end

  test "creating a movement with negative quantity fails", %{
    conn: conn,
    user: user,
    portfolio: portfolio
  } do
    variables = %{
      "input" => %{
        "assetCode" => "MGLU3",
        "averagePrice" => "9.99",
        "date" => "2020-04-09",
        "operationalCost" => "1.23",
        "portfolioId" => portfolio.id,
        "quantity" => -10,
        "type" => "SALE"
      }
    }

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @create_movement_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "data" => %{
               "createMovement" => %{
                 "errors" => [%{"key" => "quantity", "message" => "must be greater than 0"}],
                 "success" => false,
                 "movement" => nil
               }
             }
           }
  end

  test "creating a movement with invalid type fails", %{
    conn: conn,
    user: user,
    portfolio: portfolio
  } do
    variables = %{
      "input" => %{
        "assetCode" => "MGLU3",
        "averagePrice" => "9.99",
        "date" => "2020-04-09",
        "operationalCost" => "1.23",
        "portfolioId" => portfolio.id,
        "quantity" => 10,
        "type" => "INVALID"
      }
    }

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @create_movement_mutation, variables: variables)

    assert json_response(conn, 200) == %{
             "errors" => [
               %{
                 "message" =>
                   "Argument \"input\" has invalid value $input.\nIn field \"type\": Expected type \"MovementsType!\", found \"INVALID\".",
                 "locations" => []
               }
             ]
           }
  end

  test "creating a movement without logged user fails", %{conn: conn, portfolio: portfolio} do
    variables = %{
      "input" => %{
        "assetCode" => "MGLU3",
        "averagePrice" => "9.99",
        "date" => "2020-04-09",
        "operationalCost" => "1.23",
        "portfolioId" => portfolio.id,
        "quantity" => 10,
        "type" => "PURCHASE"
      }
    }

    conn = post(conn, "/api/", query: @create_movement_mutation, variables: variables)

    assert %{
             "data" => %{"createMovement" => nil},
             "errors" => [
               %{
                 "message" => "unauthorized"
               }
             ]
           } = json_response(conn, 200)
  end
end
