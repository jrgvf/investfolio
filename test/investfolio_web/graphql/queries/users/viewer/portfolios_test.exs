defmodule InvestfolioWeb.GraphQL.Queries.Users.Viewer.PortfoliosTest do
  use InvestfolioWeb.ConnCase, async: true

  alias Investfolio.Factory

  @portfolios_query """
  query {
    viewer {
      portfolios {
        id
        name
        description
        status
        portfolioUsers {
          userId
          userName
          userEmail
          role
        }
        movements {id assetCode}
        dividends {id assetCode}
      }
    }
  }
  """
  test "portfolios viewer field returns list of portfolio", %{conn: conn} do
    user = Factory.create_user()
    user2 = Factory.create_user()
    portfolio = Factory.create_portfolio(user)
    asset = Factory.create_asset("MXRF11", :fii)
    movement = Factory.create_movement(asset, portfolio.id)
    dividend = Factory.create_dividend(asset, portfolio.id)
    Factory.create_portfolio_user(user2.id, portfolio.id, :reader)

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @portfolios_query)

    assert %{
             "data" => %{
               "viewer" => %{"portfolios" => [portfolio_data]}
             }
           } = json_response(conn, 200)

    assert %{
             "id" => portfolio.id,
             "name" => portfolio.name,
             "description" => portfolio.description,
             "status" => "ACTIVE",
             "movements" => [
               %{"id" => movement.id, "assetCode" => "MXRF11"}
             ],
             "dividends" => [
               %{"id" => dividend.id, "assetCode" => "MXRF11"}
             ],
             "portfolioUsers" => [
               %{
                 "userId" => user.id,
                 "userName" => user.name,
                 "userEmail" => user.email,
                 "role" => "OWNER"
               },
               %{
                 "userId" => user2.id,
                 "userName" => user2.name,
                 "userEmail" => user2.email,
                 "role" => "READER"
               }
             ]
           } == portfolio_data
  end
end
