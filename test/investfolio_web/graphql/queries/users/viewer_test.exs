defmodule InvestfolioWeb.GraphQL.Queries.Users.ViewerTest do
  use InvestfolioWeb.ConnCase, async: true

  @viewer_query """
  query {
    viewer {
      id
      name
      email
    }
  }
  """
  test "viewer field returns user", %{conn: conn} do
    user = Investfolio.Factory.create_user()

    conn =
      conn
      |> auth_user(user)
      |> post("/api/", query: @viewer_query)

    assert %{
             "data" => %{
               "viewer" => viewer_data
             }
           } = json_response(conn, 200)

    assert %{"id" => user.id, "name" => user.name, "email" => user.email} == viewer_data
  end

  test "viewer field returns unauthorized error", %{conn: conn} do
    conn =
      conn
      |> post("/api/", query: @viewer_query)

    assert %{
             "data" => %{
               "viewer" => nil
             },
             "errors" => [%{"message" => "unauthorized"}]
           } = json_response(conn, 200)
  end
end
