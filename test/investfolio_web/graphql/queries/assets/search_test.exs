defmodule InvestfolioWeb.GraphQL.Queries.Assets.SearchTest do
  use InvestfolioWeb.ConnCase, async: true

  alias Investfolio.Factory

  setup do
    mglu = Factory.create_asset("MGLU3", :stock)
    mxrf = Factory.create_asset("MXRF11", :fii)
    {:ok, %{mglu: mglu, mxrf: mxrf}}
  end

  @query """
  query	{
    search(matching: "Fake MXRF11") {
      code
      type
      name
      companyName
    }
  }
  """
  test "search field returns list of asset by similarity", %{conn: conn, mxrf: mxrf, mglu: mglu} do
    conn = post(conn, "/api/", query: @query)

    assert %{"data" => %{"search" => [mxrf_data, mglu_data]}} = json_response(conn, 200)

    assert %{
             "code" => mxrf.code,
             "type" => "FII",
             "name" => mxrf.name,
             "companyName" => mxrf.company_name
           } == mxrf_data

    assert %{
             "code" => mglu.code,
             "type" => "STOCK",
             "name" => mglu.name,
             "companyName" => mglu.company_name
           } == mglu_data
  end

  @query """
  query	{
    search(matching: "MGLU3") {
      code
      type
      name
      companyName
    }
  }
  """
  test "search field returns list of asset by code", %{conn: conn, mglu: mglu} do
    conn = post(conn, "/api/", query: @query)

    assert %{"data" => %{"search" => [mglu_data]}} = json_response(conn, 200)

    assert %{
             "code" => mglu.code,
             "type" => "STOCK",
             "name" => mglu.name,
             "companyName" => mglu.company_name
           } == mglu_data
  end
end
