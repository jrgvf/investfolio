defmodule InvestfolioWeb.GraphQL.Queries.Assets.AssetsTest do
  use InvestfolioWeb.ConnCase, async: true

  alias Investfolio.Factory

  setup do
    mglu = Factory.create_asset("MGLU3", :stock)
    mxrf = Factory.create_asset("MXRF11", :fii)
    {:ok, %{mglu: mglu, mxrf: mxrf}}
  end

  @query """
  query	{
    assets {
      code
      type
      name
      companyName
    }
  }
  """
  test "assets field returns list of asset", %{conn: conn, mglu: mglu, mxrf: mxrf} do
    conn = post(conn, "/api/", query: @query)

    assert %{"data" => %{"assets" => [mglu_data, mxrf_data]}} = json_response(conn, 200)

    assert %{
             "code" => mglu.code,
             "type" => "STOCK",
             "name" => mglu.name,
             "companyName" => mglu.company_name
           } == mglu_data

    assert %{
             "code" => mxrf.code,
             "type" => "FII",
             "name" => mxrf.name,
             "companyName" => mxrf.company_name
           } == mxrf_data
  end

  @query """
  query	{
    assets (order: NAME_DESC) {
      code
      type
      name
      companyName
    }
  }
  """
  test "assets field returns list of asset by name DESC", %{conn: conn, mglu: mglu, mxrf: mxrf} do
    conn = post(conn, "/api/", query: @query)

    assert %{"data" => %{"assets" => [mxrf_data, mglu_data]}} = json_response(conn, 200)

    assert %{
             "code" => mglu.code,
             "type" => "STOCK",
             "name" => mglu.name,
             "companyName" => mglu.company_name
           } == mglu_data

    assert %{
             "code" => mxrf.code,
             "type" => "FII",
             "name" => mxrf.name,
             "companyName" => mxrf.company_name
           } == mxrf_data
  end

  @query """
  query	{
    assets (filter: {type: FII}) {
      code
      type
      name
      companyName
    }
  }
  """
  test "assets field returns list of asset filtered by type", %{conn: conn, mxrf: mxrf} do
    conn = post(conn, "/api/", query: @query)

    assert %{"data" => %{"assets" => [mxrf_data]}} = json_response(conn, 200)

    assert %{
             "code" => mxrf.code,
             "type" => "FII",
             "name" => mxrf.name,
             "companyName" => mxrf.company_name
           } == mxrf_data
  end

  @query """
  query	{
    assets (filter: {code: "mglu"}) {
      code
      type
      name
      companyName
    }
  }
  """
  test "assets field returns list of asset filtered by code", %{conn: conn, mglu: mglu} do
    conn = post(conn, "/api/", query: @query)

    assert %{"data" => %{"assets" => [mglu_data]}} = json_response(conn, 200)

    assert %{
             "code" => mglu.code,
             "type" => "STOCK",
             "name" => mglu.name,
             "companyName" => mglu.company_name
           } == mglu_data
  end
end
