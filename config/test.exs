use Mix.Config

# Configure your database
config :investfolio, Investfolio.Repo,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  database: System.get_env("POSTGRES_DB") || "investfolio_test",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :investfolio, InvestfolioWeb.Endpoint,
  http: [port: 4002],
  server: false

# Pbkdf2 adjust the complexity, and time taken, of the password hashing calculation
config :pbkdf2_elixir, :rounds, 1

# Print only warnings and errors during test
config :logger, level: :error

# Use mock adapter for all clients
config :tesla, Investfolio.Uol.Client, adapter: Tesla.Mock

config :investfolio, Investfolio.RabbitMQ.Manager,
  virtual_host: nil,
  host: System.get_env("RABBITMQ_HOST") || "localhost"
