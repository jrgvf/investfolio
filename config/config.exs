# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :investfolio,
  ecto_repos: [Investfolio.Repo]

# Configures migration options
config :investfolio, Investfolio.Repo,
  migration_primary_key: [
    name: :id,
    type: :binary_id,
    autogenerate: false,
    read_after_writes: true,
    default: {:fragment, "uuid_generate_v4()"}
  ],
  migration_timestamps: [type: :utc_datetime_usec]

# Configures the endpoint
config :investfolio, InvestfolioWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "a7AlbHt28UMbJW10YyUrnEKT25ZuoJwUb7cdfs/2cpYk8v4CN7s+7BOChfmLG5gD",
  render_errors: [view: InvestfolioWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Investfolio.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "e5bL8TXg"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :investfolio, Investfolio.Auth,
  issuer: "investfolio",
  ttl: {7, :days},
  verify_issuer: true,
  secret_key: "QweiR11pPTt1eLtzou/v/kRD7EpD31H7uAz1FpFZnDAPg6Y/+tOzHdcTtl0IQDnV",
  serializer: Investfolio.Auth

config :guardian, Guardian.DB,
  repo: Investfolio.Repo,
  sweep_interval: 60

config :investfolio, Investfolio.RabbitMQ.Manager,
  virtual_host: "investfolio",
  queues: [uol_assets: "uol.assets", upsert_assets: "upsert.assets"]

config :lager,
  error_logger_redirect: false,
  handlers: [level: :critical]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
