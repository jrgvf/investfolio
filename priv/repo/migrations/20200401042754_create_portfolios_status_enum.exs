defmodule Investfolio.Repo.Migrations.CreatePortfoliosStatusEnum do
  use Ecto.Migration

  alias Investfolio.Portfolios.StatusEnum

  def up do
    StatusEnum.create_type()
  end

  def down do
    StatusEnum.drop_type()
  end
end
