defmodule Investfolio.Repo.Migrations.CreateMovements do
  use Ecto.Migration

  alias Investfolio.Movements.TypeEnum

  def change do
    create table(:movements) do
      add :asset_code, :string
      add :type, TypeEnum.type()
      add :isin, :string
      add :quantity, :integer
      add :description, :text
      add :date, :date
      add :average_price, :decimal
      add :operational_cost, :decimal
      add :total_value, :decimal
      add :portfolio_id, references(:portfolios, on_delete: :delete_all)
      add :asset_id, references(:assets, on_delete: :nothing)

      timestamps()
    end

    create index(:movements, [:portfolio_id])
    create index(:movements, [:asset_id])
    create index(:movements, [:portfolio_id, :asset_code])

    execute """
    CREATE OR REPLACE FUNCTION update_total_value()
    RETURNS trigger AS $$
    BEGIN
      IF (NEW.type = 'purchase') THEN
        NEW.total_value := (NEW.average_price * NEW.quantity) + NEW.operational_cost;
        RETURN NEW;
      ELSIF (NEW.type = 'sale') THEN
        NEW.total_value := (NEW.average_price * NEW.quantity) - NEW.operational_cost;
        RETURN NEW;
      END IF;
      RETURN NULL;
    END;
    $$ LANGUAGE plpgsql;
    """

    execute """
    DROP TRIGGER IF EXISTS update_total_value_trg ON movements;
    """

    execute """
    CREATE TRIGGER update_total_value_trg
    BEFORE INSERT OR UPDATE
    ON movements
    FOR EACH ROW
    EXECUTE PROCEDURE update_total_value();
    """
  end
end
