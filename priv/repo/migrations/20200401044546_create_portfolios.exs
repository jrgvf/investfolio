defmodule Investfolio.Repo.Migrations.CreatePortfolios do
  use Ecto.Migration

  alias Investfolio.Portfolios.StatusEnum

  def change do
    create table(:portfolios) do
      add :name, :string
      add :description, :text
      add :status, StatusEnum.type()

      timestamps()
    end
  end
end
