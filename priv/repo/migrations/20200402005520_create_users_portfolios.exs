defmodule Investfolio.Repo.Migrations.CreateUsersPortfolios do
  use Ecto.Migration

  alias Investfolio.Portfolios.RoleEnum

  def change do
    create table(:portfolios_users) do
      add :role, RoleEnum.type()
      add :user_id, references(:users, on_delete: :delete_all)
      add :portfolio_id, references(:portfolios, on_delete: :delete_all)

      timestamps()
    end

    create index(:portfolios_users, [:user_id])
    create index(:portfolios_users, [:portfolio_id])
  end
end
