defmodule Investfolio.Repo.Migrations.AddGuardianDbIndexForExp do
  use Ecto.Migration

  def change do
    create index(:guardian_tokens, [:exp])
  end
end
