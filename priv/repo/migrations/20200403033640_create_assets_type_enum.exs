defmodule Investfolio.Repo.Migrations.CreateAssetsTypeEnum do
  use Ecto.Migration

  alias Investfolio.Assets.TypeEnum

  def up do
    TypeEnum.create_type()
  end

  def down do
    TypeEnum.drop_type()
  end
end
