defmodule Investfolio.Repo.Migrations.CreateAssets do
  use Ecto.Migration

  alias Investfolio.Assets.TypeEnum

  def change do
    create table(:assets) do
      add :external_id, :string
      add :code, :string
      add :name, :string
      add :company_name, :string
      add :company_abv_name, :string
      add :type, TypeEnum.type()

      timestamps()
    end

    create unique_index(:assets, [:code])

    execute "CREATE EXTENSION IF NOT EXISTS pg_trgm;"

    execute "CREATE INDEX assets_code_trgm_index ON assets USING gin (code gin_trgm_ops);"

    execute "CREATE INDEX assets_name_trgm_index ON assets USING gin (name gin_trgm_ops);"

    execute "CREATE INDEX assets_company_name_trgm_index ON assets USING gin (company_name gin_trgm_ops);"

    execute "CREATE INDEX assets_company_abv_name_trgm_index ON assets USING gin (company_abv_name gin_trgm_ops);"
  end
end
