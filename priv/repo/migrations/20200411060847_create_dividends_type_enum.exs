defmodule Investfolio.Repo.Migrations.CreateDividendsTypeEnum do
  use Ecto.Migration

  alias Investfolio.Dividends.TypeEnum

  def up do
    TypeEnum.create_type()
  end

  def down do
    TypeEnum.drop_type()
  end
end
