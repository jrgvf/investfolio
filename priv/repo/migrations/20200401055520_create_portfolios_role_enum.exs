defmodule Investfolio.Repo.Migrations.CreatePortfoliosRoleEnum do
  use Ecto.Migration

  alias Investfolio.Portfolios.RoleEnum

  def up do
    RoleEnum.create_type()
  end

  def down do
    RoleEnum.drop_type()
  end
end
