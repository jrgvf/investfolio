defmodule Investfolio.Repo.Migrations.CreateDividends do
  use Ecto.Migration

  alias Investfolio.Dividends.TypeEnum

  def change do
    create table(:dividends) do
      add :asset_code, :string
      add :type, TypeEnum.type()
      add :isin, :string
      add :quantity, :integer
      add :value, :decimal
      add :base_date, :date
      add :payment_date, :date
      add :total_received, :decimal
      add :portfolio_id, references(:portfolios, on_delete: :nothing)
      add :asset_id, references(:assets, on_delete: :nothing)
      add :reference_month, :integer
      add :reference_year, :integer

      timestamps()
    end

    create index(:dividends, [:portfolio_id])
    create index(:dividends, [:asset_id])
    create index(:dividends, [:portfolio_id, :asset_code])

    execute """
    CREATE OR REPLACE FUNCTION update_total_received_and_references()
    RETURNS trigger AS $$
    BEGIN
      NEW.total_received := (NEW.value * NEW.quantity);
      NEW.reference_month := EXTRACT(MONTH FROM NEW.base_date);
      NEW.reference_year := EXTRACT(YEAR FROM NEW.base_date);
      RETURN NEW;
    END;
    $$ LANGUAGE plpgsql;
    """

    execute """
    DROP TRIGGER IF EXISTS update_total_received_and_references_trg ON dividends;
    """

    execute """
    CREATE TRIGGER update_total_received_and_references_trg
    BEFORE INSERT OR UPDATE
    ON dividends
    FOR EACH ROW
    EXECUTE PROCEDURE update_total_received_and_references();
    """
  end
end
