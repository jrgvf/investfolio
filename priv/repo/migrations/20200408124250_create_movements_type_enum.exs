defmodule Investfolio.Repo.Migrations.CreateMovementsTypeEnum do
  use Ecto.Migration

  alias Investfolio.Movements.TypeEnum

  def up do
    TypeEnum.create_type()
  end

  def down do
    TypeEnum.drop_type()
  end
end
